//
//  FontFamily.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 27.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit

public enum FontFamily {
    
    public enum Avenir: String, Fontable {
        case book = "Book"
        case medium = "Medium"
        case light = "Light"
        case heavy = "Heavy"
        
        public static let baseName: String = "Avenir"
    }
    
    public enum AvenirNext: String, Fontable {
        case regular = "Regular"
        case medium = "Medium"
        case ultraLight = "UltraLight"
        
        public static let baseName: String = "AvenirNext"
    }
    
}

public protocol Fontable {
    var rawValue: String { get }
    static var baseName: String { get }
}

public extension Fontable {
    func font(withSize size: CGFloat) -> UIFont {
        let name = Self.baseName + "-" + self.rawValue
        guard let font = UIFont(name: name, size: size) else {
            fatalError("Cannot get font with name: \(name)")
        }
        
        return font
    }
}
