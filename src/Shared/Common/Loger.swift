//
//  Loger.swift
//  Shared
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxMVVM

#if DEBUG
public let log: Loger = LogerFactory().setup(logLevel: .debug).make()
#else
public let log: Loger = LogerFactory().setup(logLevel: .info).make()
#endif

