//
//  Configuration.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 27.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import DynamicColor

public struct Configuration {
    private init() { }
    
    public static func statusBarStyle(isLightMode: Bool) -> UIStatusBarStyle {
        if isLightMode == true {
            return .default
        }
        
        return .lightContent
    }
    
    private struct ColorConsts {
        
        static let primaryDark: UIColor = UIColor(hexString: "2B2B2B")
        static let primaryLight: UIColor = UIColor(hexString: "FFFFFF")
        
        static var primaryColor: UIColor {
            //Yeah, it's smelly I know. Color switch came up later.
//            if UserProvider.shared.colorMode.isLight {
//                return primaryDark
//            }
            
            return primaryLight
        }
        static var primaryBackground: UIColor {
//            if UserProvider.shared.colorMode.isLight {
//                return primaryLight
//            }
            
            return primaryDark
        }
        static let flatGreen: UIColor = UIColor(hexString: "43A047")
        static let flatRed: UIColor = UIColor(hexString: "CF000F")
    }
    
    public enum Fonts: String, UnwrappableEnum {
        
        case navigationTitle
        case barButtonTitle
        
        //MARK: - Text
        case primaryTitle
        case primaryText
        case secondaryText
        
        //MARK: - Button
        case primaryButton
        
        //MARK: - Pie Chart
        case chartHours
        case chartMaxHours
        
        //MARK: - Time Entry
        case timeEntryTitle
        
        public func with(size: CGFloat) -> UIFont {
            let font: Fontable
            
            switch self {
            case .navigationTitle: font = FontFamily.Avenir.light
            case .barButtonTitle: font = FontFamily.Avenir.medium
                
            case .primaryTitle: font = FontFamily.AvenirNext.ultraLight
            case .primaryText: font = FontFamily.Avenir.book
            case .secondaryText: font = FontFamily.Avenir.book
                
            case .primaryButton: font = FontFamily.AvenirNext.regular
                
            case .chartHours: font = FontFamily.Avenir.medium
            case .chartMaxHours: font = FontFamily.Avenir.light
                
            case .timeEntryTitle: font = FontFamily.Avenir.medium
            }
            
            return font.font(withSize: size)
        }
    }
    
    public enum Colors: String, UnwrappableEnum {
        
        case brand
        case line
        case border
        case dotBorder
        case dotSelected
        case primaryBackground
        case flatGreen
        case flatRed
        
        //MARK: - Text
        case primaryTitle
        case primaryText
        case secondaryText
        
        //MARK: - Button
        case primaryButtonTitle
        case primaryButtonBackground
        
        case secondaryButtonTitle
        case secondaryButtonBackground
        
        case logoutButtonTitle
        
        //MARK: - Time Entry
        case timeEntryTitle
        case maxHoursText
        
        //MARK: - Dashboard
        case dashboardSettingsSectionImageTint
        
        //MARK: - FSCalendar
        case selection
        case weekdayText
        case headerTitle
        case titleDefault
        case titleSelection
        
        public var color: UIColor {
            switch self {
            case .brand: return ColorConsts.primaryColor
            case .line: return ColorConsts.primaryColor.withAlphaComponent(0.5)
            case .border: return ColorConsts.primaryColor
            case .dotBorder: return ColorConsts.primaryColor
            case .dotSelected: return ColorConsts.primaryColor
            case .primaryBackground: return ColorConsts.primaryBackground
            case .flatGreen: return ColorConsts.flatGreen
            case .flatRed: return ColorConsts.flatRed
                
            case .primaryTitle: return ColorConsts.primaryColor
            case .primaryText: return ColorConsts.primaryColor
            case .secondaryText: return ColorConsts.primaryColor.withAlphaComponent(0.6)
                
            case .primaryButtonTitle: return ColorConsts.primaryBackground
            case .primaryButtonBackground: return ColorConsts.primaryColor
                
            case .secondaryButtonTitle: return ColorConsts.primaryColor
            case .secondaryButtonBackground: return ColorConsts.primaryBackground
                
            case .logoutButtonTitle: return ColorConsts.flatRed
                
            case .timeEntryTitle: return ColorConsts.primaryColor.withAlphaComponent(0.8)
            case .maxHoursText: return ColorConsts.primaryColor
                
            case .dashboardSettingsSectionImageTint: return ColorConsts.primaryColor.withAlphaComponent(0.6)
                
            case .selection: return ColorConsts.primaryColor
            case .weekdayText: return ColorConsts.primaryColor
            case .headerTitle: return ColorConsts.primaryColor
            case .titleDefault: return ColorConsts.primaryColor
            case .titleSelection: return ColorConsts.primaryBackground
            }
        }
    }
}

public protocol UnwrappableEnum {
    init?(rawValue: String)
}

public extension UnwrappableEnum {
    
    init(raw: String) {
        guard let type = Self(rawValue: raw) else {
            fatalError("Cannot init with raw: \(raw)")
        }
        
        self = type
    }
}
