//
//  ConfigUtils.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxMVVM

public class ConfigUtils: Utils {
    
    private struct Keys {
        static let ClientId: String = "ClientId"
        static let ClientSecret: String = "ClientSecret"
        static let AuthUrl: String = "AuthUrl"
        static let ApiUrl: String = "ApiUrl"
    }
    
    var plistPath: String {
        return R.file.appSettingsPlist.path() !! "Path can't be nil"
    }
    
    public static var ClientId: String {
        return ConfigUtils().value(forKey: Keys.ClientId)
    }
    
    public static var ClientSecret: String {
        return ConfigUtils().value(forKey: Keys.ClientSecret)
    }
    
    public static var AuthUrl: String {
        return ConfigUtils().value(forKey: Keys.AuthUrl)
    }
    
    public static var ApiUrl: String {
        return ConfigUtils().value(forKey: Keys.ApiUrl)
    }
}
