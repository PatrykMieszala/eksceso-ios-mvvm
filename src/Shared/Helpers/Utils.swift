//
//  Utils.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 10.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

protocol Utils {
    
    var plistPath: String { get }
}

extension Utils {
    func value<T>(forKey key: String) -> T {
        guard
            let dict = NSDictionary(contentsOfFile: plistPath),
            let value = dict[key] as? T
            else {
                fatalError("Error when getting value for key: \(key)")
        }
        
        return value
    }
}
