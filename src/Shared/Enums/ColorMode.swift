//
//  ColorMode.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 21/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public enum ColorMode: Int, Codable, CaseIterable {
    
    case light = 0
    case dark
    
    public var isLight: Bool {
        if case .light = self { return true }
        
        return false
    }
    
    public var text: String {
        switch self {
        case .light: return "Light"
        case .dark: return "Dark"
        }
    }
}
