//
//  DashboardSettingsOption.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 08.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import SwiftDate

public enum IntervalOption: Codable, Equatable {
    
    public enum Error: Swift.Error {
        case couldntDecode
    }
    
    case thisWeek
    case thisMonth
    case lastMonth
    case customMonth(Date)
    
    public var from: Date {
        switch self {
        case .thisWeek:
            return Date().dateAtStartOf(.weekday)
        case .thisMonth:
            return Date().dateAtStartOf(.month)
        case .lastMonth:
            let month = Date().month - 1
            let lastMonth = Date().dateBySet([.day: 1, .month: month])
            let date = lastMonth?.dateAtStartOf(.month)
            
            return date ?? Date()
        case .customMonth(let month):
            return month.dateAtStartOf(.month)
        }
    }
    
    public var to: Date {
        switch self {
        case .thisWeek:
            return Date()
        case .thisMonth:
            return Date()
        case .lastMonth:
            let month = Date().month - 1
            let lastMonth = Date().dateBySet([.day: 1, .month: month])
            let date = lastMonth?.dateAtEndOf(.month)
            
            return date ?? Date()
        case .customMonth(let month):
            return month.dateAtEndOf(.month)
        }
    }
    
    public var text: String {
        switch self {
        case .thisWeek: return "This week"
        case .thisMonth: return "This month"
        case .lastMonth: return "Last month"
        case .customMonth: return "Custom month"
        }
    }
    
    public static func allCasesWhenCustom(month: Date) -> [IntervalOption] {
        return [.thisWeek, .thisMonth, .lastMonth, .customMonth(month)]
    }
    
    private var encodeValue: String {
        switch self {
        case .thisWeek, .thisMonth, .lastMonth: return text
        case .customMonth(let month):
            return text + String(month.timeIntervalSince1970)
        }
    }
    
    private init?(fromEncodedString string: String) {
        switch string {
        case IntervalOption.thisWeek.text: self = .thisWeek
        case IntervalOption.thisMonth.text: self = .thisMonth
        case IntervalOption.lastMonth.text: self = .lastMonth
        default:
            let text = IntervalOption.customMonth(Date()).text
            guard string.contains(text) else { return nil }
            
            let timeIntervalString = string.replacingOccurrences(of: text, with: "")
            
            guard let timeInterval = TimeInterval(timeIntervalString) else {
                return nil
            }
            
            let month = Date(timeIntervalSince1970: timeInterval)
            
            self = IntervalOption.customMonth(month)
        }
    }
    
    public init(from decoder: Decoder) throws {
        let stringEncoded = try decoder.singleValueContainer().decode(String.self)
        guard let encoded = IntervalOption(fromEncodedString: stringEncoded) else {
            throw Error.couldntDecode
        }
        
        self = encoded
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.encodeValue)
    }
}

public func ==(lhs: IntervalOption, rhs: IntervalOption) -> Bool {
    switch lhs {
    case .thisWeek:
        if case .thisWeek = rhs {
            return true
        }
        
        return false
    case .thisMonth:
        if case .thisMonth = rhs {
            return true
        }
        
        return false
    case .lastMonth:
        if case .lastMonth = rhs {
            return true
        }
        
        return false
    case .customMonth:
        if case .customMonth = rhs {
            return true
        }
        
        return false
    }
}
