//
//  RxCommandProtocol+WeakListener.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation
import RxSwift
import RxCocoa

public typealias RxListenerMethod<TParent: DisposeBagProvider, TElement> = (TParent) -> (TElement) -> Void
public typealias RxListener<TParent: DisposeBagProvider, TElement> = (parent: TParent, method: RxListenerMethod<TParent, TElement>)

extension ObservableConvertibleType {
    
    ///Method for subscribeNext as `weak` listener
    ///
    /// - parameter parent: Generic `TParent` type conforming to `DisposeBagProvider` protocol
    /// - parameter method: @escaping generic `RxListenerMethod<TParent, Observable.Element>`
    func weakListener<TParent: DisposeBagProvider>(
        parent: TParent,
        method: @escaping RxListenerMethod<TParent, E>
        )
    {
        self
            .asObservable()
            .subscribeNext { [weak parent] (element) in
                guard let parent = parent else { return }
                method(parent)(element)
            }
            .dispose(in: parent.disposeBag)
    }
}

///Operator for subscribeNext as `weak` listener for RxCommandProtocol.success
///
/// - parameter success: Generic `Success<TCommandOutput>` type for `RxCommandProtocol.success` property
/// - parameter listener: Generic `RxListener<TParent, TCommandOutput>`
public func += <TCommandOutput, TParent: DisposeBagProvider, TSuccess: Observable<TCommandOutput>>(
    success: TSuccess,
    listener: RxListener<TParent, TCommandOutput>
    )
{
    success.weakListener(parent: listener.parent, method: listener.method)
}

///Operator for subscribeNext as `weak` listener for Void RxCommandProtocol.success
///
/// - parameter success: `Success<Void>` type for `RxCommandProtocol.success` property
/// - parameter listener: Generic `RxListener<TParent, Void>`
public func += <TParent: DisposeBagProvider, TSuccess: Observable<Void>>(
    success: TSuccess,
    listener: RxListener<TParent, Void>
    )
{
    success.weakListener(parent: listener.parent, method: listener.method)
}

///Operator for subscribeNext as `weak` listener for `RxCommandProtocol.executing`
///
/// - parameter executing: Generic `Executing` type for `RxCommandProtocol.executing` property
/// - parameter listener: Generic `RxListener<TParent, RxCommandProtocolStateType>`
public func += <TParent: DisposeBagProvider>(
    executing: RxCommandProtocol.CommandState,
    listener: RxListener<TParent, RxCommandState>
    )
{
    executing.weakListener(parent: listener.parent, method: listener.method)
}

///Operator for subscribeNext as `weak` listener for `RxCommandProtocol.error`
///
/// - parameter error: Generic `Error` type for `RxCommandProtocol.error` property
/// - parameter listener: Generic `RxListener<TParent, Error>`
public func += <TParent: DisposeBagProvider>(
    error: RxCommandProtocol.CommandError,
    listener: RxListener<TParent, Error>
    )
{
    error.weakListener(parent: listener.parent, method: listener.method)
}
