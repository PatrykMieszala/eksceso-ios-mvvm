//
//  RxCommandState.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

///Enum indicating command state
public enum RxCommandState {
    ///Command is executing
    case executing
    ///Command finished execution
    case finish
}
