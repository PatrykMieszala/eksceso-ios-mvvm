//
//  RxCommandProtocol.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation
import RxSwift

public protocol RxCommandProtocol {
    
    ///Generic execution parameter type
    associatedtype TCommandInput
    ///Generic success parameter type
    associatedtype TCommandOutput
    
    ///Command success typealias. Declared for += operator overloading.
    typealias CommandSuccess                = Observable<TCommandOutput>
    ///Command error typealias. Declared for += operator overloading.
    typealias CommandError                  = Observable<Error>
    ///Command executing typealias. Declared for += operator overloading.
    typealias CommandState                  = Observable<RxCommandState>
    
    ///Command method typealias
    typealias CommandMethod                = (TCommandInput) -> Single<TCommandOutput>
    
    ///Command state fired on RxCommandStateType change
    var state:      CommandState            { get }
    ///Command state fired on command success
    var success:    CommandSuccess          { get }
    ///Command state fired on command error
    var error:      CommandError            { get }
    
    ///Executes command method.
    /// - parameter param: Generic input parameter.
    /// - parameter dispose: Dispose command disposeBag when execute. False by default.
    @discardableResult
    func execute(_ param: TCommandInput, dispose: Bool) -> Single<TCommandOutput>
}

public extension RxCommandProtocol {
    ///Executes command handler.
    /// - parameter param: Generic input parameter.
    /// - parameter dispose: Dispose command disposeBag when execute. False by default.
    @discardableResult
    public func execute(_ param: TCommandInput) -> Single<TCommandOutput> {
        return execute(param, dispose: false)
    }
}

public extension RxCommandProtocol where TCommandInput == Void {
    
    @discardableResult
    public func execute() -> Single<TCommandOutput> {
        return execute(())
    }
    
    @discardableResult
    public func execute(dispose: Bool) -> Single<TCommandOutput> {
        return execute((), dispose: dispose)
    }
}
