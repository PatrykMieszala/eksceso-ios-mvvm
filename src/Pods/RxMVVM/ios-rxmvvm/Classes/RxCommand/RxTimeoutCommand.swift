//
//  RxTimeoutCommand.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation
import RxSwift

public class RxTimeoutCommand<TCommandInput, TCommandOutput>: RxCommand<TCommandInput, TCommandOutput> {
    
    private let timeout: Double
    
    public init(
        file: String = #file,
        function: String = #function,
        line: Int = #line,
        timeout: Double,
        _ method: @escaping CommandMethod
        ) {
        
        self.timeout = timeout
        super.init(file: file, function: function, line: line, method)
    }
    
    deinit {
        self.log("\(type(of: self)) :: deinit")
    }
    
    @discardableResult
    override public func execute(_ param: TCommandInput, dispose: Bool) -> Single<TCommandOutput> {
        
        if dispose {
            disposeBag = DisposeBag()
        }
        
        log("\(type(of: self)) :: executing with parameter type: \(type(of: param)), timeout: \(timeout)")
        self.statePublish.onNext(.executing)
        
        let command = self.method(param).timeout(timeout, scheduler: MainScheduler.instance)
        
        command
            .observeOn(MainScheduler.instance)
            .doOnNext { (args) in
                self.log("\(type(of: self)) :: success with argument type: \(type(of: args))")
                self.successPublish.onNext(args)
            }
            .map { _ in return }
            //catchError because errors are unsubscribing whole chain
            .catchError { (error) -> Single<Void> in
                self.log("\(type(of: self)) :: error: \(type(of: error))")
                self.errorPublish.onNext(error)
                
                return .just()
            }
            .subscribeOn(MainScheduler.instance)
            .subscribeCompleted {
                self.log("\(type(of: self)) :: finished")
                self.statePublish.onNext(.finish)
            }
            .dispose(in: disposeBag)
        
        return command
    }
}
