//
//  RxCommand.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation
import RxSwift

public class RxCommand<TCommandInput, TCommandOutput>: RxCommandProtocol {
    
    public var success: Observable<TCommandOutput> {
        return successPublish
    }
    
    public var state: RxCommandProtocol.CommandState {
        return statePublish
    }
    
    public var error: RxCommandProtocol.CommandError {
        return errorPublish
    }
    
    public private(set) var lastError: Error?
    
    internal let method: CommandMethod
    
    internal let file: String
    internal let function: String
    internal let line: Int
    
    internal let successPublish: PublishSubject<TCommandOutput> = .init()
    internal let statePublish: PublishSubject<RxCommandState> = .init()
    internal let errorPublish: PublishSubject<Error> = .init()
    
    internal var disposeBag = DisposeBag()
    
    public init(
        file: String = #file,
        function: String = #function,
        line: Int = #line,
        _ method: @escaping CommandMethod
        ) {
        
        self.method = method
        self.file = file
        self.function = function
        self.line = line
    }
    
    deinit {
        self.log("\(type(of: self)) :: deinit")
    }
    
    @discardableResult
    public func execute(_ param: TCommandInput, dispose: Bool) -> Single<TCommandOutput> {
        
        if dispose {
            disposeBag = DisposeBag()
        }
        
        self.log("\(type(of: self)) :: executing with parameter: \(param)")
        self.statePublish.onNext(.executing)
        
        let command = self.method(param)
        
        command
            .observeOn(MainScheduler.instance)
            .doOnNext { (args) in
                self.log("\(type(of: self)) :: ✅ success with argument: \(args)")
                self.successPublish.onNext(args)
            }
            //map to Void, to make catching errors possible
            .map { _ in return }
            //catchError because errors are unsubscribing whole chain
            .catchError { (error) -> Single<Void> in
                self.log("\(type(of: self)) :: ‼️ error: \(error)")
                self.errorPublish.onNext(error)
                self.lastError = error
                
                return .just()
            }
            .subscribeOn(MainScheduler.instance)
            .subscribeCompleted {
                self.log("\(type(of: self)) :: finished")
                self.statePublish.onNext(.finish)
            }
            .dispose(in: disposeBag)
        
        return command
    }
    
    internal func log<T>(_ value: T) {
        prettyPrint(value, logLevel: .verbose, file: file, function: function, line: line, source: .command)
    }
}

public extension ObservableType {
    
    ///Binds `ObservableType` to command. On `event.next` command is fired.
    /// - parameter command: `RxCommand`
    public func bind<TEventArgs>(to command: RxCommand<Self.E, TEventArgs>) -> Disposable {
        return subscribe { event in
            switch event {
            case let .next(element):
                command.execute(element)
            case let .error(error):
                let error = "Binding error to command: \(error)"
                fatalError(error)
            case .completed:
                break
            }
        }
    }
    
    ///Binds `ObservableType` to command. On `event.next` command is fired and disposeBag of command disposed.
    /// - parameter command: `RxCommand`
    /// - parameter dispose: `Bool`
    public func bind<TEventArgs>(to command: RxCommand<Self.E, TEventArgs>, dispose: Bool) -> Disposable {
        return subscribe { event in
            switch event {
            case let .next(element):
                command.execute(element, dispose: dispose)
            case let .error(error):
                let error = "Binding error to command: \(error)"
                fatalError(error)
            case .completed:
                break
            }
        }
    }
}
