//
//  RxViewModelState.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

///Enum indicating operation state
public enum RxOperationState {
    ///Operation is executing
    case executing
    ///Operation finished execution
    case finished
}
