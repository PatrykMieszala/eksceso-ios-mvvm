//
//  Driver+RxOperationState.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import RxSwift
import RxCocoa

public extension Driver {
    
    static func rxOperationState<T1, T2>(trigger: Driver<T1>, execution: Driver<T2>) -> Driver<RxOperationState> {
        
        return Driver
            .merge([
                trigger
                    .map { _ in return RxOperationState.executing },
                execution
                    .map { _ in return  RxOperationState.finished }
                    .debounce(0.1)
                ])
    }
    
    static func rxOperationState<T1, T2>(trigger: Driver<T1>, execution: Observable<T2>) -> Driver<RxOperationState> {
        
        return Driver
            .merge([
                trigger
                    .map { _ in return RxOperationState.executing },
                execution
                    .map { _ in return  RxOperationState.finished }
                    .asDriver(onErrorJustReturn: .finished)
                    .debounce(0.1)
                ])
    }
    
    static func rxOperationState<T>(trigger: Driver<Void>, execution: Observable<T>) -> Driver<RxOperationState> {
        
        return Driver
            .merge([
                trigger
                    .map { _ in return RxOperationState.executing },
                execution
                    .map { _ in return  RxOperationState.finished }
                    .asDriver(onErrorJustReturn: .finished)
                    .debounce(0.1)
                ])
    }
    
    static func rxOperationState<T1, T2>(trigger: Observable<T1>, execution: Observable<T2>) -> Driver<RxOperationState> {
        
        return Driver
            .merge([
                trigger
                    .map { _ in return RxOperationState.executing }
                    .asDriver(onErrorJustReturn: .executing),
                execution
                    .map { _ in return  RxOperationState.finished }
                    .asDriver(onErrorJustReturn: .finished)
                    .debounce(0.1)
                ])
    }
    
    static func rxOperationState<T1, T2>(trigger: Observable<T1>, execution: Driver<T2>) -> Driver<RxOperationState> {
        
        return Driver
            .merge([
                trigger
                    .map { _ in return RxOperationState.executing }
                    .asDriver(onErrorJustReturn: .executing),
                execution
                    .map { _ in return  RxOperationState.finished }
                    .debounce(0.1)
                ])
    }
}
