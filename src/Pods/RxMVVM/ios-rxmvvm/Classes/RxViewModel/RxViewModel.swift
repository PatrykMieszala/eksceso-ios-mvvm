//
//  RxViewModel.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//
import RxSwift
import RxCocoa

public protocol RxViewModel: DisposeBagProvider {
}

public extension RxViewModel {
    
    ///Method checks internet connection, fires up preparation and executes `method`. Throws error if some step fails.
    func execute<TInput, TResult>(
        preparation: () throws -> TInput,
        method: @escaping (TInput) -> Single<TResult>
        ) -> Single<TResult> {
        
        if Reachability.isConnectedToNetwork() == false {
            return .error(RxViewModelError.noNetwork)
        }
        do {
            let params = try preparation()
            
            return method(params)
        }
        catch let error {
            return .error(error)
        }
    }
    
    ///Method checks internet connection and executes `method`. Throws error if some step fails.
    func execute<TInput, TResult>(
        params: TInput,
        method: (TInput) -> Single<TResult>
        ) -> Single<TResult> {
        
        if Reachability.isConnectedToNetwork() == false {
            return .error(RxViewModelError.noNetwork)
        }
        
        return method(params)
    }
    
    ///Method checks internet connection and executes `method`. Throws error if some step fails.
    static func execute<TInput, TResult>(
        params: TInput,
        method: (TInput) -> Single<TResult>
        ) -> Single<TResult> {
        
        if Reachability.isConnectedToNetwork() == false {
            return .error(RxViewModelError.noNetwork)
        }
        
        return method(params)
    }
    
    ///Method checks internet connection and executes `method` with `Void` input. Throws error if some step fails.
    func execute<TResult>(
        method: () -> Single<TResult>
        ) -> Single<TResult> {

        return execute(params: (), method: method)
    }
    
    ///Method checks internet connection and executes `method` with `Void` input. Throws error if some step fails.
    static func execute<TResult>(
        method: () -> Single<TResult>
        ) -> Single<TResult> {
        
        if Reachability.isConnectedToNetwork() == false {
            return .error(RxViewModelError.noNetwork)
        }
        
        return method()
    }
    
    
    ///Method creates `RxCommand` from given method reference. Memory leak safe.
    public func command<TCommandInput, TCommandOutput>(
        method: @escaping (Self) -> (TCommandInput) -> Single<TCommandOutput>,
        file: String = #file,
        function: String = #function,
        line: Int = #line
        ) -> RxCommand<TCommandInput, TCommandOutput> {
        
        return RxCommand(file: file, function: function, line: line) { [unowned self] param in
            return method(self)(param)
        }
    }
    
    ///Special method for creating `RxCommand` based on Void input. Memory leak safe.
    public func command<TCommandInput, TCommandOutput>(
        method: @escaping (Self) -> () -> Single<TCommandOutput>,
        file: String = #file,
        function: String = #function,
        line: Int = #line
        ) -> RxCommand<TCommandInput, TCommandOutput> {
        
        return RxCommand(file: file, function: function, line: line) { [unowned self] param in
            return method(self)()
        }
    }
    
    ///Method creates `RxTimeoutCommand` from given method reference. Memory leak safe.
    public func command<TCommandInput, TCommandOutput>(
        timeout: Double,
        method: @escaping (Self) -> (TCommandInput) -> Single<TCommandOutput>,
        file: String = #file,
        function: String = #function,
        line: Int = #line
        ) -> RxTimeoutCommand<TCommandInput, TCommandOutput> {
        
        return RxTimeoutCommand(file: file, function: function, line: line, timeout: timeout) { [unowned self] param in
            return method(self)(param)
        }
    }
    
    
    ///Special method for creating `RxTimeoutCommand` based on Void input. Memory leak safe.
    public func command<TCommandInput, TCommandOutput>(
        timeout: Double,
        method: @escaping (Self) -> () -> Single<TCommandOutput>,
        file: String = #file,
        function: String = #function,
        line: Int = #line
        ) -> RxTimeoutCommand<TCommandInput, TCommandOutput> {
        
        return RxTimeoutCommand(file: file, function: function, line: line, timeout: timeout) { [unowned self] param in
            return method(self)()
        }
    }
}
