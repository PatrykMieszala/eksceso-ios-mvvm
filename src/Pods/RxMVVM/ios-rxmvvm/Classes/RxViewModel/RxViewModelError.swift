//
//  RxViewModelError.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 06.03.2018.
//

import Foundation

enum RxViewModelError: Swift.Error {
    case noNetwork
}
