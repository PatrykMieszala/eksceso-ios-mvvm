//
//  UIDatePicker+Rx.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

#if os(iOS)
    import UIKit
    
    extension Reactive where Base: UIDatePicker {
        
        ///Reactive extension for UIDatePickerModel
        public var pickerMode: Binder<UIDatePicker.Mode> {
            return Binder(self.base) { picker, mode in
                picker.datePickerMode = mode
            }
        }
    }
    
#endif
