//
//  UICollectionView+Rx.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import RxSwift
import RxCocoa

#if os(iOS)
    import UIKit
    
    extension Reactive where Base: UICollectionView {
        public var itemSelected: ControlEvent<(UICollectionView, IndexPath)> {
            let source = self.delegate.methodInvoked(#selector(UICollectionViewDelegate.collectionView(_:didSelectItemAt:)))
                .map { a in
                    return (a[0] as! UICollectionView, a[1] as! IndexPath)
            }
            
            return ControlEvent(events: source)
        }
    }
#endif

