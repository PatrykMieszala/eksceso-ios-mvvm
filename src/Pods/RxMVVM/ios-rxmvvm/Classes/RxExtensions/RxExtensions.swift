//
//  RxExtensions.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

public extension ObservableType {
    
    public func doOnNext(_ next: @escaping ((E) -> Void)) -> Observable<E> {
        return self.do(onNext: next)
    }
    
    public func doOnCompleted(_ completed: @escaping (() -> Void)) -> Observable<E> {
        return self.do(onCompleted: completed)
    }
    
    public func doOnError(_ error: @escaping ((Error) -> Void)) -> Observable<E> {
        return self.do(onError: error)
    }
    
    public func subscribeNext(_ next: @escaping ((E) -> Void)) -> Disposable {
        return self.subscribe(onNext: next)
    }
    
    public func subscribeError(_ error: @escaping ((Error) -> Void)) -> Disposable {
        return self.subscribe(onError: error)
    }
    
    public func subscribeCompleted(_ completed: @escaping (() -> Void)) -> Disposable {
        return self.subscribe(onCompleted: completed)
    }
}

extension Observable where Element == Void {
    
    public class func just() -> Observable<Element> {
        return Observable.just(())
    }
}

public extension SharedSequenceConvertibleType where Self.SharingStrategy == RxCocoa.DriverSharingStrategy {
    
    public func driveNext(_ next: @escaping ((E) -> Void)) -> Disposable {
        return self.drive(onNext: next)
    }
    
    public func driveCompleted(_ completed: @escaping (() -> Void)) -> Disposable {
        return self.drive(onCompleted: completed)
    }
    
    public func driveDisposed(_ disposed: @escaping (() -> Void)) -> Disposable {
        return self.drive(onDisposed: disposed)
    }
}

public extension SharedSequence {
    
    public func doOnNext(_ next: @escaping ((E) -> Void)) -> SharedSequence {
        return self.do(onNext: next)
    }
    
    public func doOnCompleted(_ completed: @escaping (() -> Void)) -> SharedSequence {
        return self.do(onCompleted: completed)
    }
    
    public func doOnSubscribe(_ subscribe: @escaping ((() -> Void))) -> SharedSequence {
        return self.do(onSubscribe: subscribe)
    }
    
    public func doOnSubscribed(_ subscribed: @escaping (() -> Void)) -> SharedSequence {
        return self.do(onSubscribed: subscribed)
    }
    
    public func doOnDispose(_ dispose: @escaping (() -> Void)) -> SharedSequence {
        return self.do(onDispose: dispose)
    }
}

extension Driver where Element == Void {
    
    public static func just() -> Driver<Element> {
        return Driver.just(())
    }
}


extension PublishSubject where Element == Void {
    
    public func onNext() {
        self.onNext(())
    }
}

extension ControlEvent {
    public func subscribeNext(_ next: @escaping ((E) -> Void)) -> Disposable {
        return self.subscribe(onNext: next)
    }
}

extension Disposable {
    public func dispose(in bag: DisposeBag) {
        disposed(by: bag)
    }
}
