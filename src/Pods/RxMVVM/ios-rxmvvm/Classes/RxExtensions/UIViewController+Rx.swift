//
//  UIViewController+Rx.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//
#if os(iOS)
    import UIKit
    
    import RxSwift
    import RxCocoa
    
    public extension Reactive where Base: UIViewController {
        public var viewDidLoad: ControlEvent<Void> {
            let source = methodInvoked(#selector(Base.viewDidLoad))
                .map { _ in }
            
            return ControlEvent(events: source)
        }
        
        public var viewWillAppear: ControlEvent<Bool> {
            let source = methodInvoked(#selector(Base.viewWillAppear))
                .map { $0.first as? Bool ?? false }
            
            return ControlEvent(events: source)
        }
        public var viewDidAppear: ControlEvent<Bool> {
            let source = methodInvoked(#selector(Base.viewDidAppear))
                .map { $0.first as? Bool ?? false }
            
            return ControlEvent(events: source)
        }
        
        public var viewWillDisappear: ControlEvent<Bool> {
            let source = methodInvoked(#selector(Base.viewWillDisappear))
                .map { $0.first as? Bool ?? false }
            
            return ControlEvent(events: source)
        }
        public var viewDidDisappear: ControlEvent<Bool> {
            let source = methodInvoked(#selector(Base.viewDidDisappear))
                .map { $0.first as? Bool ?? false }
            
            return ControlEvent(events: source)
        }
        
        public var viewPoped: Observable<Void> {
            return viewDidDisappear
                .map { _ in return }
                .filter { [weak base] _ in
                    return base?.isMovingFromParent == true
                }
        }
        
        public var viewDismissed: Observable<Void> {
            return viewDidDisappear
                .map { _ in return }
                .filter { [weak base] _ in
                    return base?.isBeingDismissed == true
            }
        }
        
    }
#endif
