//
//  SingleExtensions.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

public extension PrimitiveSequenceType where TraitType == SingleTrait {
    
    public func doOnNext(_ onNext: @escaping ((ElementType) throws -> Void)) -> PrimitiveSequence<SingleTrait, ElementType> {
        return self.do(onNext: onNext)
    }
    
    public func doOnError(_ onError: @escaping ((Error) throws -> Void)) -> PrimitiveSequence<SingleTrait, ElementType> {
        return self.do(onError: onError)
    }
    
    public func subscribeCompleted(_ completed: @escaping ((ElementType) -> Void)) -> Disposable {
        return self.subscribe(onSuccess: completed)
    }
}

public extension Single {
    
    public func catchErrorJustReturn(_ element: Element) -> Single<Element> {
        return self
            .asObservable()
            .catchErrorJustReturn(element)
            .asSingle()
    }
}

public extension Single where Element == Void {
    
    public static func just() -> Single<Element> {
        return Single.just(())
    }
    
    public func catchErrorJustReturn() -> PrimitiveSequence<Trait, Void> {
        return self.catchError { (error) -> PrimitiveSequence<Trait, Void> in
            let ps = Single.just(()).primitiveSequence
            
            //This force cast is (should be) safe. For some weird reson `catchError` requires return `PrimitiveSequence<Trait, Element>`, even tho we are always in area of `PrimitiveSequence<SingleTrait, Element>` when working with Single
            return ps as! PrimitiveSequence<Trait, Element>
        }
    }
}
