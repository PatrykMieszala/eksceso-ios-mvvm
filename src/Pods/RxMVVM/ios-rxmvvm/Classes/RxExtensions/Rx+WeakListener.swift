//
//  Rx+WeakListener.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

import RxSwift
import RxCocoa

///Operator for subscribeNext as `weak` listener for Driver
///
/// - parameter success: Generic `Driver<TElement>`
/// - parameter listener: Generic `RxListener<TParent, TElement>`
public func += <TParent: DisposeBagProvider, TElement>(
    state: Driver<TElement>,
    listener: RxListener<TParent, TElement>
    )
{
    state.weakListener(parent: listener.parent, method: listener.method)
}
