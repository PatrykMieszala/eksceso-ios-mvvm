//
//  UIControl+Rx.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

#if os(iOS)
    import UIKit
    
    extension Reactive where Base: UIControl {
        public static func valuePublic<T, ControlType: UIControl>(_ control: ControlType, getter:  @escaping (ControlType) -> T, setter: @escaping (ControlType, T) -> ()) -> ControlProperty<T> {
            let values: Observable<T> = Observable.deferred { [weak control] in
                guard let existingSelf = control else {
                    return Observable.empty()
                }
                
                return (existingSelf as UIControl).rx.controlEvent([.allEditingEvents, .valueChanged])
                    .flatMap { _ in
                        return control.map { Observable.just(getter($0)) } ?? Observable.empty()
                    }
                    .startWith(getter(existingSelf))
            }
            return ControlProperty(values: values, valueSink: Binder(control) { control, value in
                setter(control, value)
            })
        }
    }
    
#endif
