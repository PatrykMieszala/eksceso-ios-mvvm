//
//  UITableView+Rx.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

#if os(iOS)
    import UIKit
    
    extension Reactive where Base: UITableView {
        public var itemSelected: ControlEvent<(UITableView, IndexPath)> {
            let source = self.delegate.methodInvoked(#selector(UITableViewDelegate.tableView(_:didSelectRowAt:)))
                .map { a in
                    return (a[0] as! UITableView, a[1] as! IndexPath)
            }
            
            return ControlEvent(events: source)
        }
    }
#endif
