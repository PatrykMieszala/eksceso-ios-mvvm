//
//  Rx+OnlySuccess.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

import RxSwift
import RxCocoa
import RxOptional

public extension ObservableType {
    
    func ignoreErrors() -> Observable<E> {
        return self
            .map { element -> E? in
                return element
            }
            .catchError { (error) in
                return .just(nil)
            }
            .filterNil()
    }
    
    func asDriverIgnoreErrors() -> Driver<E> {
        return self
            .map { element -> E? in
                return element
            }
            .asDriver(onErrorJustReturn: nil)
            .filterNil()
    }
}

