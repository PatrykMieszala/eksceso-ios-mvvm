//
//  Rx+OnlyErrors.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

import RxSwift
import RxCocoa
import RxOptional

public extension ObservableType {
    
    func onlyErrors() -> Observable<Error> {
        return self
            .map { _ -> Error? in
                return nil
            }
            .catchError { (error) in
                return .just(error)
            }
            .filterNil()
    }
    
    func asDriverOnlyErrors() -> Driver<Error> {
        return self
            .map { _ -> Error? in
                return nil
            }
            .catchError { (error) in
                return .just(error)
            }
            .asDriver(onErrorJustReturn: nil)
            .filterNil()
    }
}
