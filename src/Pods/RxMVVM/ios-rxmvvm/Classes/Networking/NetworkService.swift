//
//  NetworkService.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import RxSwift
import Alamofire

open class NetworkService: Networking {
    
    private typealias Error = NetworkServiceError
    
    public func get<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> PrimitiveSequence<SingleTrait, T> where T : Decodable {
        return request(method: .get, path: path, parameters: parameters, encoding: encoding)
    }
    open func post<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return request(method: .post, path: path, parameters: parameters, encoding: encoding)
    }
    open func patch<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return request(method: .patch, path: path, parameters: parameters, encoding: encoding)
    }
    open func put<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return request(method: .put, path: path, parameters: parameters, encoding: encoding)
    }
    open func delete<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return request(method: .delete, path: path, parameters: parameters, encoding: encoding)
    }
    
    // MARK: - Public Members
    open let session: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        
        return SessionManager(configuration: configuration)
    }()
    
    open let urlBuilder: URLBuilder.Type
    
    open var decoder: JSONDecoder {
        return JSONDecoder()
    }
    
    open var encoder: JSONEncoder {
        return JSONEncoder()
    }
    
    open var successRange: Range<HTTPStatusCode> {
        return .ok ..< .nonAuthoritativeInformation
    }
    
    // MARK: - Constructor(s)
    public init(urlBuilder: URLBuilder.Type) {
        self.urlBuilder = urlBuilder
    }
    
    // MARK: - Private Methods
    // MARK: - Request method for single mappable response
    ///Request method for single mappable response
    open func request<T>(method: Alamofire.HTTPMethod, path: String, parameters: [String: Any]? = nil, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        if !Reachability.isConnectedToNetwork() {
            return .error(Error.noNetwork)
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let paramsData = try? JSONSerialization.data(withJSONObject: parameters ?? [:], options: .prettyPrinted)
        self.log(type: .request(method), url: path, parameters: paramsData)
        
        return Single.create { [weak self] (observer) -> Disposable in
            
            guard let `self` = self else {
                return Disposables.create()
            }
            
            let request: DataRequest = self.session.request(self.urlBuilder.url(forPath: path), method: method, parameters: parameters, encoding: encoding, headers: self.headers())
                .responseData { [weak self] (response) in
                    guard let `self` = self else { return }
                    self.handler(observer: observer, response: response, map: self.map)
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    // MARK: - Handler for mappable response
    ///Handler for mappable response
    open func handler<T>(observer: (SingleEvent<T>) -> (), response: DataResponse<Data>, map: (Data) throws -> T?) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        self.log(type: .response, url: response.request?.url?.absoluteString ?? "", parameters: response.value)
        
        guard response.response?.statusCode != 401 else {
            
            let error = Error.unauthorized(response.response?.statusCode)
            log(error, level: .error)
            observer(.error(error))
            
            return
        }
        
        guard response.response?.statusCode != 204 else {
            // success but no content
            let error = Error.noContent(response.response?.statusCode)
            log(error, level: .error)
            observer(.error(error))
            
            return
        }
        
        switch response.result {
        case .success(let value):
            
            if let code = response.response?.statusCode,
                let statusCode = HTTPStatusCode(rawValue: code),
                successRange.contains(statusCode) == false {
                
                let error = Error.error(response.response?.statusCode)
                log(error, level: .error)
                observer(.error(error))
                
                return
                
            }
            
            if T.self is EmptyResponseModel.Type, response.response?.statusCode == 200 {
                observer(.success(EmptyResponseModel() as! T))
                return
            }
            
            do {
                if let json = try map(value) {
                    observer(.success(json))
                    return
                }
            }
            catch let error {
                
                log(error, level: .error)
                observer(.error(error))
                
                return
            }
            
            let error = Error.error(response.response?.statusCode)
            log(error, level: .error)
            observer(.error(error))
            
            return
            
        case .failure(let error):
            
            guard response.result.error?._code != NSURLErrorTimedOut else {
                observer(.error(Error.timeout(response.response?.statusCode)))
                return
            }
            
            log(error, level: .error)
            let error = Error.error(response.response?.statusCode)
            log(error, level: .error)
            observer(.error(error))
            
            return
        }
    }
    
    // MARK: - Logging method
    open func log(type: NetworkServiceLogType, url: String, parameters: Data?) {
        var message = ""
        message.append(contentsOf: " 📡 API \(type.description)\n\nURL: \(url)")
        
        if let data = parameters,
            let jsonString = String(data: data, encoding: .utf8) {
            message.append(contentsOf: "\nPARAMETERS:\n" + jsonString)
        }
        
        if case .response = type {
            log(message, terminator: "\n", level: .verbose)
        } 
        else {
            log(message, terminator: "\n", level: .debug)
        }
    }
    
    func log<T>(_ value: T, terminator: String = "", level: LogLevel) {
        prettyPrint(value, terminator: terminator, logLevel: level, source: nil)
    }
    
    // MARK: - Single type mapper
    ///Single type mapper
    open func map<T>(data: Data) throws -> T where T : Decodable {
        return try decoder.decode(T.self, from: data)
    }
    
    // MARK: - Headers
    ///Method which creates headers included in Alamofire request. By default returns empty struct.
    open func headers() -> HTTPHeaders {
        return HTTPHeaders()
    }
}
