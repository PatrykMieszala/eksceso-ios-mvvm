//
//  EmptyResponse.swift
//  
//
//  Created by Patryk Mieszała on 23.02.2018.
//

import Foundation
    
///Dummy class, which represents empty network response.
public class EmptyResponseModel: Codable {
    
    // MARK: - Public Properties
    
    public init() { }
}
