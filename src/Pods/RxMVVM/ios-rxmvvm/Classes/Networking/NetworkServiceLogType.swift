//
//  NetworkServiceLogType.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import Foundation
import Alamofire

public enum NetworkServiceLogType: CustomStringConvertible {
    
    case request(HTTPMethod)
    case response
    
    public var description: String {
        switch self {
        case .request(let method): return "REQUEST \(method.rawValue) ➡️"
        case .response: return "RESPONSE ⬅️"
        }
    }
}
