//
//  EncodableExtensions.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 12.03.2018.
//

import Foundation

public extension Encodable {
    
    func toJSONDict() -> [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else { return nil}
        
        return json
    }
    
    func toJSONString() -> String? {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        guard let data = try? encoder.encode(self) else { return nil }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toJSONData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
