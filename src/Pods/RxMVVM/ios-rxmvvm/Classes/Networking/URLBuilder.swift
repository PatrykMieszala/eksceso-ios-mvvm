//
//  IURLProvider.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 23.02.2018.
//

///Protocol for url builder.
public protocol URLBuilder {
    ///Method creates url with given path
    static func url(forPath path: String) -> String
}
