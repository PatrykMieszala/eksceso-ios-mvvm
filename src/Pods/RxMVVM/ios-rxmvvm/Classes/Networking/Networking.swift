//
//  Networking.swift
//  
//
//  Created by Patryk Mieszała on 23.02.2018.
//

import RxSwift
import Alamofire

public protocol Networking {
    func get<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable
    func post<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable
    func patch<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable
    func put<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable
    func delete<T>(path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable
}

public extension Networking {
    public func get<T>(path: String) -> Single<T> where T : Decodable {
        return get(path: path, parameters: nil, encoding: URLEncoding.default)
    }
    public func post<T>(path: String) -> Single<T> where T : Decodable {
        return post(path: path, parameters: nil, encoding: URLEncoding.default)
    }
    public func patch<T>(path: String) -> Single<T> where T : Decodable {
        return patch(path: path, parameters: nil, encoding: URLEncoding.default)
    }
    public func put<T>(path: String) -> Single<T> where T : Decodable {
        return put(path: path, parameters: nil, encoding: URLEncoding.default)
    }
    public func delete<T>(path: String) -> Single<T> where T : Decodable {
        return delete(path: path, parameters: nil, encoding: URLEncoding.default)
    }
    
    public func get<T>(path: String, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return get(path: path, parameters: nil, encoding: encoding)
    }
    public func post<T>(path: String, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return post(path: path, parameters: nil, encoding: encoding)
    }
    public func patch<T>(path: String, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return patch(path: path, parameters: nil, encoding: encoding)
    }
    public func put<T>(path: String, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return put(path: path, parameters: nil, encoding: encoding)
    }
    public func delete<T>(path: String, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return delete(path: path, parameters: nil, encoding: encoding)
    }
}
