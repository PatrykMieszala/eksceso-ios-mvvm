//
//  NetworkServiceError.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import Foundation

public enum NetworkServiceError: Swift.Error, CustomStringConvertible {
    
    case noNetwork
    case timeout(Int?)
    case unauthorized(Int?)
    case noContent(Int?)
    case error(Int?)
    
    public var text: String? {
        get {
            switch self {
            case .noNetwork:
                return "Error: no network"
            case .timeout:
                return "Error: timeout"
            case .unauthorized:
                return "Error: unauthorized"
            case .noContent:
                return "Error: no content"
            case .error:
                return "Error: other"
            }
        }
    }
    
    public var isNoContent: Bool {
        switch self {
        case .noContent: return true
        default: return false
        }
    }
    
    public var description: String {
        return text ?? "Unknown error"
    }
    
    public var errorCode: Int? {
        switch self {
        case .timeout(let code): return code
        case .unauthorized(let code): return code
        case .noContent(let code): return code
        case .error(let code): return code
        default: return nil
        }
    }
}
