//
//  RxCollectionViewWireframe.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 08.03.2018.
//

import RxSwift
import RxCocoa

public protocol CollectionViewWireframe {
    
    var itemSelected: ControlEvent<(UICollectionView, IndexPath)> { get }
    
    func items<DataSource, O>(dataSource: DataSource) -> (O) -> Disposable where DataSource : RxCollectionViewDataSourceType, DataSource : UICollectionViewDataSource, O : ObservableType, DataSource.Element == O.E
    
    func modelSelected<T>(_ modelType: T.Type) -> ControlEvent<T>
    func modelDeselected<T>(_ modelType: T.Type) -> ControlEvent<T>
    func model<T>(at indexPath: IndexPath) throws -> T
}

public class CollectionViewWireframeImplementation: CollectionViewWireframe {
    
    public var itemSelected: ControlEvent<(UICollectionView, IndexPath)> {
        return collectionView.itemSelected
    }
    
    private let collectionView: Reactive<UICollectionView>
    
    init(collectionView: Reactive<UICollectionView>) {
        self.collectionView = collectionView
    }
    
    public func items<DataSource, O>(dataSource: DataSource) -> (O) -> Disposable where DataSource : RxCollectionViewDataSourceType, DataSource : UICollectionViewDataSource, O : ObservableType, DataSource.Element == O.E {
        return collectionView.items(dataSource: dataSource)
    }
    
    public func modelSelected<T>(_ modelType: T.Type) -> ControlEvent<T> {
        return collectionView.modelSelected(modelType)
    }
    
    public func modelDeselected<T>(_ modelType: T.Type) -> ControlEvent<T> {
        return collectionView.modelDeselected(modelType)
    }
    
    public func model<T>(at indexPath: IndexPath) throws -> T {
        return try collectionView.model(at: indexPath)
    }
}

public extension Reactive where Base == UICollectionView {
    
    var wireframe: CollectionViewWireframe {
        return CollectionViewWireframeImplementation(collectionView: self)
    }
    
}
