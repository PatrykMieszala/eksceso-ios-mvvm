//
//  RxTableViewWireframe.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

import RxSwift
import RxCocoa

public protocol TableViewWireframe {
    
    var itemSelected: ControlEvent<(UITableView, IndexPath)> { get }
    var itemMoved: ControlEvent<ItemMovedEvent> { get }
    
    func items<
        DataSource: RxTableViewDataSourceType & UITableViewDataSource,
        O: ObservableType>
        (dataSource: DataSource)
        -> (_ source: O)
        -> Disposable
    where DataSource.Element == O.E
    
    func modelSelected<T>(_ modelType: T.Type) -> ControlEvent<T>
    func modelDeselected<T>(_ modelType: T.Type) -> ControlEvent<T>
    func modelDeleted<T>(_ modelType: T.Type) -> ControlEvent<T>
    func model<T>(at indexPath: IndexPath) throws -> T
}

public class TableViewWireframeImplementation: TableViewWireframe {
    
    public var itemSelected: ControlEvent<(UITableView, IndexPath)> {
        return tableView.itemSelected
    }
    public var itemMoved: ControlEvent<ItemMovedEvent> {
        return tableView.itemMoved
    }
    
    private let tableView: Reactive<UITableView>
    
    init(tableView: Reactive<UITableView>) {
        self.tableView = tableView
    }
    
    public func items<DataSource, O>(dataSource: DataSource) -> (O) -> Disposable where DataSource : RxTableViewDataSourceType, DataSource : UITableViewDataSource, O : ObservableType, DataSource.Element == O.E {
        return tableView.items(dataSource: dataSource)
    }
    
    public func modelSelected<T>(_ modelType: T.Type) -> ControlEvent<T> {
        return tableView.modelSelected(modelType)
    }
    
    public func modelDeselected<T>(_ modelType: T.Type) -> ControlEvent<T> {
        return tableView.modelDeselected(modelType)
    }
    
    public func modelDeleted<T>(_ modelType: T.Type) -> ControlEvent<T> {
        return tableView.modelDeleted(modelType)
    }
    
    public func model<T>(at indexPath: IndexPath) throws -> T {
        return try tableView.model(at: indexPath)
    }
}

public extension Reactive where Base == UITableView {
    
    var wireframe: TableViewWireframe {
        return TableViewWireframeImplementation(tableView: self)
    }
    
}
