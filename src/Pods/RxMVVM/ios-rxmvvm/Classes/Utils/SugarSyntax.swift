//
//  SugarSyntax.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 16.02.2018.
//

import Foundation

infix operator !!

/**
 Function tries to unwrap wrapped value. If fails, throws global fatal error.
 
 Example:
 
 let value: Int? = 1
 let unwrappedValue = value !! "Error when trying to get value"
**/
public func !!<T>(wrapped: T?, failureText :@autoclosure () -> String) -> T {
    if let x = wrapped { return x }
    fatalError(failureText())
}
