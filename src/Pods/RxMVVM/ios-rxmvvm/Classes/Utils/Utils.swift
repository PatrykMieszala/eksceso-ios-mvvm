//
//  Utils.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation

public var RxMVVMSettings = RxMVVMSettingsStruct()

///Struct which contains settings of `RxMVVM`
public struct RxMVVMSettingsStruct {
    
    ///Variable indicates settings for logging. If `true`, `RxMVVM` will log some events.
    public var logEnabled: Bool = false
    
    ///External logging method. By default, uses Loger.
    public var logger: ( (UtilsLogEntry) -> () ) = { entry in
        internalLoger.log(entry.value, level: entry.logLevel, terminator: entry.terminator ?? "", file: entry.file, function: entry.function, line: entry.line)
    }
    
    init() { }
}

private let internalLoger = LogerFactory().setup(logLevel: .error).make()

private let C = RxMVVMSettings

internal func prettyPrint(_ value: Any..., terminator: String = "", logLevel: LogLevel, file: String = #file, function: String = #function, line: Int = #line, source: Source? = nil) {
    
    guard C.logEnabled == true else { return }
    
    let time: String = {
        let date: Date = Date()
        
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        let text = df.string(from: date)
        
        return text
    }()
    
    var message = "(\(time))"
    
    if let source = source {
        message.append(" \(source.description) ")
    }
    
    message.append(" - \(value)")
    
    let entry = UtilsLogEntry(defaultMessage: message,
                              value: value,
                              terminator: terminator,
                              file: file,
                              function: function,
                              line: line,
                              source: source,
                              logLevel: logLevel)
    
    C.logger(entry)
}

public struct UtilsLogEntry {
    
    public let defaultMessage: String
    public let value: Any
    public let terminator: String
    public let file: String
    public let function: String
    public let line: Int
    public let source: Source?
    public let logLevel: LogLevel
}

public enum Source {
    case command
    case coordinator
    
    public var description: String {
        switch self {
        case .command: return "⚙️ RxCommand"
        case .coordinator: return "☸️ RxNavigator"
        }
    }
}
