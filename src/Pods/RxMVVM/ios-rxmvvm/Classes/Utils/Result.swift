//
//  Result.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 13.03.2018.
//

public enum Result<T> {
    case ok(T)
    case error(Error)
    
    public var value: T? {
        switch self {
        case .ok(let value): return value
        case .error: return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .ok: return nil
        case .error(let error): return error
        }
    }
    
    public var isSuccess: Bool {
        switch self {
        case .ok: return true
        case .error: return false
        }
    }
    
    public init(_ error: Error) {
        self = .error(error)
    }
    
    public init(_ value: T) {
        self = .ok(value)
    }
    
    public static func from<T>(errored: Result) -> Result<T> {
        switch errored {
        case .error(let error): return .error(error)
        case .ok: fatalError("This init only works with errored results")
        }
    }
}
