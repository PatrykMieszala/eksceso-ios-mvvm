//
//  UIWindow+Extensions.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 16.02.2018.
//

import UIKit

extension UIWindow {
    
    public func animate(rootViewController newRootViewController: UIViewController) {
        
        let previousViewController = rootViewController
        
        let transition = CATransition()
        transition.type = .fade
        transition.duration = 0.5
        
        // Add the transition
        layer.add(transition, forKey: kCATransition)
        
        rootViewController = newRootViewController
        
        // Update status bar appearance using the new view controllers appearance - animate if needed
        if UIView.areAnimationsEnabled {
            UIView.animate(withDuration: 0.5) {
                newRootViewController.setNeedsStatusBarAppearanceUpdate()
            }
        } else {
            newRootViewController.setNeedsStatusBarAppearanceUpdate()
        }
        
        if let transitionViewClass = NSClassFromString("UITransitionView") {
            for subview in subviews where subview.isKind(of: transitionViewClass) {
                subview.removeFromSuperview()
            }
        }
        if let presentedPrevious = previousViewController?.presentedViewController {
            // Allow the view controller to be deallocated
            presentedPrevious.dismiss(animated: false) {
                // Remove the root view in case its still showing
                presentedPrevious.view.removeFromSuperview()
            }
        }
        
        if let previousViewController = previousViewController {
            // Allow the view controller to be deallocated
            previousViewController.dismiss(animated: false) {
                // Remove the root view in case its still showing
                previousViewController.view.removeFromSuperview()
            }
        }
    }
}
