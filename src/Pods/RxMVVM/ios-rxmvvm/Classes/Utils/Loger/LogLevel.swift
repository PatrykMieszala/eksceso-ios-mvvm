//
//  LogLevel.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 06.03.2018.
//

import Foundation

public enum LogLevel: Int, CustomStringConvertible {
    
    case verbose = 0
    case debug = 1
    case info = 2
    case warning = 3
    case error = 4
    
    public var description: String {
        switch self {
        case .verbose:  return "verbose"
        case .debug:    return "debug"
        case .info:     return "info"
        case .warning:  return "warning"
        case .error:    return "error"
        }
    }
    
    func shouldLog(consideringMinimum level: LogLevel) -> Bool {
        return self.rawValue >= level.rawValue
    }
}

extension LogLevel: Comparable {
    public static func <(lhs: LogLevel, rhs: LogLevel) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
