//
//  LogOutput.swift
//  RxMVVM
//
//  Created by Patryk Mieszała on 20/10/2018.
//

import Foundation

public struct LogOutput {
    
    public let message: String
    public let terminator: String
}
