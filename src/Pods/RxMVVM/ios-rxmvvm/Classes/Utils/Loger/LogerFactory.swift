//
//  LogerFactory.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 06.03.2018.
//

import Foundation

public class LogerFactory: LogerSetup {
    
    // MARK: - Private Member(s)
    private var loger = Loger()
    
    public init() {
    }
    
    public func setup(logLevel: LogLevel, format: @escaping LogEntryFormat, endpoint: @escaping LogOutputEndpoint) -> Self {
        loger.set(level: logLevel)
        loger.set(format: format)
        loger.set(endpoint: endpoint)
        
        loger.info("Setting up Loger with level: \(logLevel).")
        
        return self
    }
    
    public func make() -> Loger {
        return loger
    }
}
