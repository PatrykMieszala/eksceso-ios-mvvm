//
//  LogEntry.swift
//  RxMVVM
//
//  Created by Patryk Mieszała on 20/10/2018.
//

import Foundation

public struct LogEntry {
    
    public let value: Any
    public let level: LogLevel
    public let file: String
    public let function: String
    public let line: Int
    
}
