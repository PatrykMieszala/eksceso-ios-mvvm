//
//  LogerSetup.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation

public protocol LogerSetup {
    
    func setup(logLevel: LogLevel, format: @escaping LogEntryFormat, endpoint: @escaping LogOutputEndpoint) -> Self
    
    func make() -> Loger
}

public extension LogerSetup {
    
    public func setup(logLevel: LogLevel) -> Self {
        return setup(logLevel: logLevel, format: Loger.LogEntryDefaultFormat, endpoint: Loger.LogOutputDefaultEndpoint)
    }
    
    public func setup(logLevel: LogLevel, endpoint: @escaping LogOutputEndpoint) -> Self {
        return setup(logLevel: logLevel, format: Loger.LogEntryDefaultFormat, endpoint: endpoint)
    }
    
    public func setup(logLevel: LogLevel, format: @escaping LogEntryFormat) -> Self {
        return setup(logLevel: logLevel, format: format, endpoint: Loger.LogOutputDefaultEndpoint)
    }
}
