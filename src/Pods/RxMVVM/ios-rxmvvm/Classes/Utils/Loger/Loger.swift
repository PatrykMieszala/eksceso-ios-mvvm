//
//  Loger.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation

public typealias LogEntryFormat = (LogEntry) -> String
public typealias LogOutputEndpoint = (LogOutput) -> Void

public class Loger: LogerProtocol {
    
    public static var LogEntryDefaultFormat: LogEntryFormat = { entry -> String in
        let date: Date = Date()
        
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        let time = df.string(from: date)
        
        let filename = ((entry.file as NSString).lastPathComponent as NSString).deletingPathExtension
        
        let message: String
        
        if let items = entry.value as? [Any] {
            message = items.reduce("") { (string, item) -> String in
                return string + "\(item) "
            }
        }
        else {
            message = "\(entry.value)"
        }
        
        return "(\(time)) \(filename).\(entry.function)[\(entry.line)] \(entry.level.description.uppercased()) - \(message)\n"
    }
    
    public static var LogOutputDefaultEndpoint: LogOutputEndpoint = { (output) in
        print(output.message, terminator: output.terminator)
    }
    
    private var minimumLogLevel: LogLevel = .info
    private var format: LogEntryFormat = LogEntryDefaultFormat
    private var endpoint: LogOutputEndpoint = LogOutputDefaultEndpoint
    
    public init() {
    }
    
    // - MARK: Internal Method(s)
    internal func set(level: LogLevel) {
        self.minimumLogLevel = level
    }
    
    internal func set(format: @escaping LogEntryFormat) {
        self.format = format
    }
    
    internal func set(endpoint: @escaping LogOutputEndpoint) {
        self.endpoint = endpoint
    }
    
    // - MARK: Public Method(s)
    public func log(_ value: Any, level: LogLevel, terminator: String = "", file: String, function: String, line: Int) {
        guard level.shouldLog(consideringMinimum: self.minimumLogLevel) == true else { return }
        
        let entry = LogEntry(value: value, level: level, file: file, function: function, line: line)
        let message = format(entry)
        
        let output = LogOutput(message: message, terminator: terminator)
        endpoint(output)
    }
    
    public func verbose(_ value: Any, terminator: String, file: String, function: String, line: Int) {
        log(value, level: .verbose, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func debug(_ value: Any, terminator: String, file: String, function: String, line: Int) {
        log(value, level: .debug, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func info(_ value: Any, terminator: String, file: String, function: String, line: Int) {
        log(value, level: .info, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func error(_ value: Any, terminator: String, file: String, function: String, line: Int) {
        log(value, level: .error, terminator: terminator, file: file, function: function, line: line)
    }
}
