//
//  LogerProtocol.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import Foundation

public protocol LogerProtocol {
    
    func verbose(_ value: Any, terminator: String, file: String, function: String, line: Int)
    func debug(_ value: Any, terminator: String, file: String, function: String, line: Int)
    func info(_ value: Any, terminator: String, file: String, function: String, line: Int)
    func error(_ value: Any, terminator: String, file: String, function: String, line: Int)
}

public extension LogerProtocol {
    
    public func verbose(_ value: Any, terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        verbose(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func debug(_ value: Any, terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        debug(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func info(_ value: Any, terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        info(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func error(_ value: Any, terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        error(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func verbose(_ value: Any..., terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        verbose(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func debug(_ value: Any..., terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        debug(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func info(_ value: Any..., terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        info(value, terminator: terminator, file: file, function: function, line: line)
    }
    
    public func error(_ value: Any..., terminator: String = "", file: String = #file, function: String = #function, line: Int = #line) {
        error(value, terminator: terminator, file: file, function: function, line: line)
    }
}
