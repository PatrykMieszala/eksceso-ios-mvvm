//
//  Either.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 13.03.2018.
//

public enum Either<T1, T2> {
    
    case left(T1)
    case right(T2)
    
    public var left: T1? {
        switch self {
        case .left(let value): return value
        case .right: return nil
        }
    }
    
    public var right: T2? {
        switch self {
        case .left: return nil
        case .right(let value): return value
        }
    }
    
    public init(_ left: T1) {
        self = .left(left)
    }
    
    public init(_ right: T2) {
        self = .right(right)
    }
    
    public static func from<T1, T2>(left: Either<T1, Any>) -> Either<T1, T2> {
        switch left {
        case .left(let left): return .left(left)
        case .right: fatalError("This init only works with left values")
        }
    }
    
    public static func from<T1, T2>(right: Either<Any, T2>) -> Either<T1, T2> {
        switch right {
        case .left: fatalError("This init only works with right values")
        case .right(let right): return .right(right)
        }
    }
}
