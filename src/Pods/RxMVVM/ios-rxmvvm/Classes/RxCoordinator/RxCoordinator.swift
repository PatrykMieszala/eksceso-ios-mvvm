//
//  RxCoordinator.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 14.02.2018.
//

import RxSwift
import RxCocoa

public typealias Route<T> = AnyObserver<T>

open class RxCoordinator: NSObject {
    
    // - MARK: Public Variable(s)
    
    ///Sub-Coordinators stored by this coordinator.
    open var coordinators: [RxCoordinator] = []
    
    // - MARK: Private Variable(s)
    fileprivate let delete: PublishSubject<Void> = PublishSubject()
    
    internal let file: String
    internal let function: String
    internal let line: Int
    
    // - MARK: Constructor(s)
    ///By calling this init, Coordinator add itself to `parent.coordinators`.
    public init(
        parent: RxCoordinator? = nil,
        file: String = #file,
        function: String = #function,
        line: Int = #line
        ) {
        
        self.file = file
        self.function = function
        self.line = line
        super.init()
        log(message: "init \(parent != nil ? "with parent: \(type(of: parent!))" : "")")
        parent?.add(coordinator: self)
    }
    
    deinit {
        log(message: "deinit")
    }
    
    #if os(iOS)
    
    // - MARK: Public Method(s)
    ///Function for start navigation. Override it!
    @discardableResult
    open func start() -> UIViewController? {
        preconditionFailure("Override in your class")
    }
    
    #elseif os(OSX)
    
    // - MARK: Public Method(s)
    ///Function for start navigation. Override it!
    @discardableResult
    open func start() -> NSViewController? {
        preconditionFailure("Override in your class")
    }
    
    #endif
    
    ///Adds coordinator to `coordinators` array. When `sub-coordinator` trigger `rx.delete`, it will automatically delete itself from `coordinators` array.
    ///
    /// - parameter coordinator: RxCoordinator
    public func add(coordinator: RxCoordinator) {
        guard coordinators.contains(coordinator) == false else {
            log(message: "trying to add already stored coordinator \(type(of: coordinator)). Aborting. ⛔️")
            return
        }
        
        log(message: "storing coordinator \(type(of: coordinator)) in coordinators array")
        
        coordinators.append(coordinator)
        
        coordinator
            .rx
            .delete
            .subscribeNext { [weak self, weak coordinator] in
                guard let `self` = self, let coordinator = coordinator else {
                    assert(false, "Problem occured when trying to delete your coordinator")
                    return
                }
                guard let index = self.coordinators.index(of: coordinator) else {
                    assert(false, "\(self) wasn't deleted, something gone wrong :(")
                    return
                }
                self.log(message: "\(type(of: coordinator)) deleted from coordinators")
                
                let deleted = self.coordinators.remove(at: index)
                deleted.clear()
            }
            .dispose(in: disposeBag)
    }
    
    ///This function returns bindable Route. Just pass handler for navigation.
    ///
    /// - parameter handler: @escaping (RxCoordinator, T) -> Void
    /// - returns: Route<T>
    public func route<T, TParent: RxCoordinator>(file: String = #file, function: String = #function, line: Int = #line, parent: TParent, _ handler: @escaping (TParent, T) -> RxCoordinator) -> Route<T> {
        return Binder(parent) { this, value in
            let coordinator = handler(this, value)
            this.log(message: "route to \(type(of: coordinator)) with parameter \(type(of: value))", file: file, function: function, line: line)
            }
            .asObserver()
    }
    
    ///This function is invoked when coordinator is deleted from `parent.coordinators`. Here you should release your references to viewControllers etc.
    open func clear() { }
    
    private func log(message: String, file: String? = nil, function: String? = nil, line: Int? = nil) {
        prettyPrint("\(type(of: self)):: \(message)", logLevel: .verbose, file: file ?? self.file, function: function ?? self.function, line: line ?? self.line, source: .coordinator)
    }
}

public extension Reactive where Base: RxCoordinator {
    ///Invoke this `PublishSubject` when `RxCoordinator` should be deleted from parent
    public var delete: PublishSubject<Void> {
        return base.delete
    }
}
