//
//  RxViewController.swift
//  ios-rxmvvm
//
//  Created by Patryk Mieszała on 07.03.2018.
//

import UIKit
import RxSwift
import RxCocoa

open class RxViewController<TViewModel>: UIViewController {
    
    ///Memory leak safe factory. It only fires up once, then it's deleted.
    public class RxViewModelFactory {
        
        public typealias Method = (RxViewController) -> TViewModel
        
        let method: (RxViewController) -> TViewModel
        
        public init(method: @escaping (RxViewController) -> TViewModel) {
            self.method = method
        }
    }
    
    public var viewModel: TViewModel {
        if let viewModel = _viewModel {
            return viewModel
        } else {
            fatalError("ViewModel must not be accessed before view loads.")
        }
    }
    
    fileprivate var factory: RxViewModelFactory!
    private var _viewModel: TViewModel?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self._viewModel = factory.method(self)
        self.factory = nil //Delete factory to prevent memory leaks.

        self.bind(viewModel: viewModel)
    }
    
    deinit {
        prettyPrint("\(type(of: self)) deinit", logLevel: .verbose)
    }
    
    open func bind(viewModel: TViewModel) {
        //To be overriden in subclass.
    }
    
    public func set(factory: @escaping () -> TViewModel) {
        let factoryMethod: RxViewModelFactory.Method = { _ in
            return factory()
        }
        
        let factory = RxViewModelFactory(method: factoryMethod)
        self.factory = factory
    }
}

extension RxViewController {
    
    public class func create<TViewController: RxViewController<TViewModel>>(
        _ viewControllerFactory: @escaping () -> TViewController,
        viewModelFactory:  @escaping (TViewController) -> TViewModel
        ) -> TViewController {
        
        let viewController: TViewController = viewControllerFactory()
        
        let factoryMethod: TViewController.RxViewModelFactory.Method = { _ in
            return viewModelFactory(viewController)
        }
        let factory = TViewController.RxViewModelFactory(method: factoryMethod)
        viewController.factory = factory
        
        return viewController
    }
    
    public class func create<TViewController: RxViewController<TViewModel>>(
        _ viewController: TViewController,
        viewModelFactory: @escaping (TViewController) -> TViewModel
        ) -> TViewController {
        
        let factoryMethod: TViewController.RxViewModelFactory.Method = { _ in
            return viewModelFactory(viewController)
        }
        let factory = TViewController.RxViewModelFactory(method: factoryMethod)
        viewController.factory = factory
        
        return viewController
    }
}
