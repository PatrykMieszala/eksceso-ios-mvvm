# ios-rxmvvm

[![CI Status](http://img.shields.io/travis/AngryNerds/ios-rxmvvm.svg?style=flat)](https://travis-ci.org/AngryNerds/ios-rxmvvm)
[![Version](https://img.shields.io/cocoapods/v/ios-rxmvvm.svg?style=flat)](http://cocoapods.org/pods/ios-rxmvvm)
[![License](https://img.shields.io/cocoapods/l/ios-rxmvvm.svg?style=flat)](http://cocoapods.org/pods/ios-rxmvvm)
[![Platform](https://img.shields.io/cocoapods/p/ios-rxmvvm.svg?style=flat)](http://cocoapods.org/pods/ios-rxmvvm)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ios-rxmvvm is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ios-rxmvvm'
```

## Author

AngryNerds, support@angrynerds.pl

## License

ios-rxmvvm is available under the MIT license. See the LICENSE file for more info.
