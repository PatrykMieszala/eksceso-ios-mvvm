//
//  HeadersProvider.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Alamofire

protocol HeadersProvider {
    func headers() -> HTTPHeaders
}
