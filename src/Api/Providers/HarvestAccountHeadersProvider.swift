//
//  HarvestAccountHeadersProvider.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Alamofire

class HarvestAccountHeadersProvider: HeadersProvider {
    
    private let accountId: Int
    
    init(accountId: Int) {
        self.accountId = accountId
    }
    
    func headers() -> HTTPHeaders {
        return ["Harvest-Account-Id": String(accountId)]
    }
}
