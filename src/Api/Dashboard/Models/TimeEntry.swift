//
//  TimeEntry.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import SwiftDate

public struct TimeEntry: Codable {
    
    public let id: Int
    
    ///"2017-03-02"
    private let spentDate: String
    
    public let hours: Double
    public let notes: String?
    public let isLocked: Bool
    public let lockedReason: String?
    public let isClosed: Bool
    public let isBilled: Bool
    
    ///Date time string "2017-08-22T17:40:24Z"
    public let timerStartedAt: String?
    ///"9:00am"
    public let startedTime: String?
    ///"9:00am"
    public let endedTime: String?
    
    public let isRunning: Bool
    public let billable: Bool
    public let budgeted: Bool
    public let billableRate: Double?
    public let costRate: Double?
    ///"created_at": "2018-06-01T10:04:48Z"
    public let createdAt: String?
    ///"updated_at": "2018-06-01T10:04:48Z"
    public let updatedAt: String?
    
    public let user: TimeEntryUser
    public let client: TimeEntryClient
    public let project: TimeEntryProject
    public let task: TimeEntryTask
    public let userAssignment: TimeEntryUserAssignment
    public let taskAssignment: TimeEntryTaskAssignment
    
    public var dateSpentDate: Date? {
        return DateInRegion(self.spentDate, format: "yyyy-MM-dd", region: Region.UTC)?.date
    }
    
    public struct TimeEntryUser: Codable {
        public let id: Int, name: String
    }
    
    public struct TimeEntryClient: Codable {
        public let id: Int, name: String, currency: String
    }
    
    public struct TimeEntryProject: Codable {
        public let id: Int, name: String, code: String
        
        public init(id: Int, name: String, code: String) {
            self.id = id
            self.name = name
            self.code = code
        }
    }
    
    public struct TimeEntryTask: Codable {
        public let id: Int, name: String
    }
    
    public struct TimeEntryUserAssignment: Codable {
        public let id: Int, isProjectManager: Bool, isActive: Bool, budget: Double?, hourlyRate: Double?
        
        ///"2018-05-08T21:53:15Z"
        public let createdAt: String
        ///"2018-05-08T21:53:15Z"
        public let updatedAt: String
    }
    
    public struct TimeEntryTaskAssignment: Codable {
        public let id: Int, billable: Bool, isActive: Bool, budget: Double?, hourlyRate: Double?
        
        ///"2018-05-08T21:53:15Z"
        public let createdAt: String
        ///"2018-05-08T21:53:15Z"
        public let updatedAt: String
    }
}
