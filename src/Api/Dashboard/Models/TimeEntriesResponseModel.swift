//
//  TimeEntriesResponseModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public struct TimeEntriesResponseModel: Codable {
    
    public let timeEntries: [TimeEntry]
    public let perPage: Int
    public let totalPages: Int
    public let totalEntries: Int
    public let nextPage: Int?
    public let previousPage: Int?
    public let page: Int
    public let links: TimeEntriesLinks
    
    public struct TimeEntriesLinks: Codable {
        public let first: String
        public let next: String?
        public let previous: String?
        public let last: String
    }
}
