//
//  DashboardApiService.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxMVVM
import Alamofire

class DashboardApiService: DashboardApi {
    
    private let api: EkscesoNetworking
    
    init(api: EkscesoNetworking) {
        self.api = api
    }
    
    func get(accountId: Int, projectId: Int?, from: Date?, to: Date?, page: Int?, perPage: Int?) -> Single<TimeEntriesResponseModel> {
        
        let propertiesOptional: [String: Any?] = [
            "project_id": projectId,
            "from": string(from: from),
            "to": string(from: to),
            "page": page,
            "per_page": perPage
        ]
        
        let properties: [String: Any] = propertiesOptional
            .compactMap { key, value -> (String, Any)? in
                guard let value = value else { return nil }
                
                return (key, value)
            }
            .reduce([:]) { (dict, args) in
                var dict = dict
                dict[args.0] = args.1
                
                return dict
        }
        
        let headersProvider = HarvestAccountHeadersProvider(accountId: accountId)
        
        return api.request(method: .get, path: "/time_entries", parameters: properties, encoding: URLEncoding.queryString, headersProvider: headersProvider)
    }
    
    private func string(from date: Date?) -> String? {
        guard let date = date else { return nil }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        return formatter.string(from: date)
    }
}
