//
//  DashboardApi.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift

public protocol DashboardApi {
    
    func get(accountId: Int, projectId: Int?, from: Date?, to: Date?, page: Int?, perPage: Int?) -> Single<TimeEntriesResponseModel>
    
}
