//
//  ApiFactory.swift
//  Api
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import Foundation

public struct ApiFactory {
    
    let tokenProvider: NetworkTokenProvider
    let tokenRefresher: NetworkTokenRefresher
    
    public init(tokenProvider: NetworkTokenProvider, tokenRefresher: NetworkTokenRefresher) {
        self.tokenProvider = tokenProvider
        self.tokenRefresher = tokenRefresher
    }
    
    func getApi(authorization: Bool = false,
                shouldRefreshToken: Bool = true)
        -> EkscesoNetworking {
            return EkscesoNetworkService(tokenProvider: tokenProvider, tokenRefresher: tokenRefresher, authorization: authorization, shouldRefreshToken: shouldRefreshToken)
    }
    
    public func getUserApi() -> UserApi {
        return UserApiService(api: getApi(authorization: true, shouldRefreshToken: false))
    }
    
    public func getAccountsApi() -> AccountsApi {
        return AccountsApiService(api: getApi(authorization: true))
    }
    
    public func getDashboardApi() -> DashboardApi {
        return DashboardApiService(api: getApi())
    }
}
