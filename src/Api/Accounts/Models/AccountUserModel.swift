//
//  AccountUserModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 01.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public struct AccountUserModel: Codable {
    
    public let id: Int
    public let firstName: String
    public let lastName: String
    public let email: String
    
}
