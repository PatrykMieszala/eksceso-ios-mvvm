//
//  AccountsResponseModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 01.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public struct AccountsResponseModel: Codable {
    
    public let user: AccountUserModel
    public let accounts: [AccountModel]
    
}
