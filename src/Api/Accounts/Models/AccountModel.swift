//
//  AccountModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 01.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public struct AccountModel: Codable, Equatable {
    
    public let id: Int
    public let name: String
    public let product: String
    public let googleSignInRequired: Bool
    
}

public func ==(lhs: AccountModel, rhs: AccountModel) -> Bool {
    return lhs.id == rhs.id
}
