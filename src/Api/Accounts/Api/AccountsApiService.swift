//
//  AccountsApiService.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 01.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxMVVM

class AccountsApiService: AccountsApi {
    
    private let api: Networking
    
    init(api: Networking) {
        self.api = api
    }
    
    public func get() -> Single<AccountsResponseModel> {
        return api.get(path: "/api/v2/accounts")
    }
}
