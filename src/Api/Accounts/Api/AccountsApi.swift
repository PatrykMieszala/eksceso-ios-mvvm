//
//  AccountsApi.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 01.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import RxMVVM

public protocol AccountsApi {
    func get() -> Single<AccountsResponseModel>
}
