//
//  UserApiService.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxMVVM
import Alamofire

import Shared

final class UserApiService: UserApi {
    
    private let api: Networking
    
    // - MARK: Constructor(s)
    init(api: Networking) {
        self.api = api
    }
    
    // - MARK: Public Method(s)
    func authorize(code: String) -> Single<UserTokenResponse> {
        let parameters: [String: Any] = [
            "code": code,
            "client_id": ConfigUtils.ClientId,
            "client_secret": ConfigUtils.ClientSecret,
            "grant_type": "authorization_code"
        ]
        
        return api.post(path: "/api/v2/oauth2/token", parameters: parameters, encoding: JSONEncoding.default)
    }
    
    func refresh(token: String) -> PrimitiveSequence<SingleTrait, UserTokenResponse> {
        let parameters: [String: Any] = [
            "refresh_token": token,
            "client_id": ConfigUtils.ClientId,
            "client_secret": ConfigUtils.ClientSecret,
            "grant_type": "refresh_token"
        ]
        
        return api.post(path: "/api/v2/oauth2/token", parameters: parameters, encoding: JSONEncoding.default)
    }
    
    // - MARK: Private Method(s)
}
