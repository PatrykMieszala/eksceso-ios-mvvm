//
//  UserApi.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift

public protocol UserApi {
    func authorize(code: String) -> Single<UserTokenResponse>
    func refresh(token: String) -> Single<UserTokenResponse>
}
