//
//  NetworkTokenProvider.swift
//  Api
//
//  Created by Patryk Mieszała on 28/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import Foundation

public protocol NetworkTokenProvider {
    
    func getToken() -> String?
}
