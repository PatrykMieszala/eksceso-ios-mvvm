//
//  EkscesoNetworkService.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxMVVM
import Alamofire

import Shared

class EkscesoNetworkService: NetworkService, EkscesoNetworking {
    
    private typealias Error = NetworkServiceError
    
    override var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return decoder
    }
    
    private let tokenProvider: NetworkTokenProvider
    private let tokenRefresher: NetworkTokenRefresher
    private let shouldRefreshToken: Bool
    
    init(
        tokenProvider: NetworkTokenProvider,
        tokenRefresher: NetworkTokenRefresher,
        authorization: Bool = false,
        shouldRefreshToken: Bool = true
        )
    {
        self.tokenProvider = tokenProvider
        self.tokenRefresher = tokenRefresher
        self.shouldRefreshToken = shouldRefreshToken
        
        super.init(urlBuilder: authorization ? EkscesoAuthURLBuilder.self : EkscesoApiURLBuilder.self)
    }
    
    func request<T>(method: HTTPMethod, path: String, parameters: [String : Any]?, encoding: ParameterEncoding, headersProvider: HeadersProvider) -> Single<T> where T : Decodable {
        
        if !Reachability.isConnectedToNetwork() {
            return .error(Error.noNetwork)
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let paramsData = try? JSONSerialization.data(withJSONObject: parameters ?? [:], options: .prettyPrinted)
        self.log(type: .request(method), url: path, parameters: paramsData)
        
        var headers = self.headers()
        
        headersProvider
            .headers()
            .forEach { key, value in
                headers[key] = value
        }
        
        return Single
            .create { [weak self] (observer) -> Disposable in
                
                guard let `self` = self else {
                    return Disposables.create()
                }
                
                let request: DataRequest = self.session.request(self.urlBuilder.url(forPath: path), method: method, parameters: parameters, encoding: encoding, headers: headers)
                    .responseData { [weak self] (response) in
                        guard let `self` = self else { return }
                        self.handler(observer: observer, response: response, map: self.map)
                }
                
                return Disposables.create {
                    request.cancel()
                }
            }
            .catchError { [weak self] error in
                guard let strongSelf = self,
                    strongSelf.shouldRefreshToken,
                    let networkError = error as? NetworkServiceError
                    else {
                        throw error
                }
                
                if case .unauthorized = networkError {
                    return strongSelf
                        .tokenRefresher
                        .refreshToken()
                        .flatMap { [weak self] in
                            guard let strongSelf = self else  {
                                throw error
                            }

                            return strongSelf.request(method: method, path: path, parameters: parameters, encoding: encoding, headersProvider: headersProvider)
                    }
                }
                
                throw error
        }
    }
    
    override func request<T>(method: HTTPMethod, path: String, parameters: [String : Any]?, encoding: ParameterEncoding) -> Single<T> where T : Decodable {
        return super
            .request(method: method, path: path, parameters: parameters, encoding: encoding)
            .catchError { [weak self] error in
                guard let strongSelf = self,
                    strongSelf.shouldRefreshToken,
                    let networkError = error as? NetworkServiceError
                    else {
                        throw error
                }
                
                if case .unauthorized = networkError {
                    return strongSelf
                        .tokenRefresher
                        .refreshToken()
                        .flatMap { [weak self] in
                            guard let strongSelf = self else  {
                                throw error
                            }

                            return strongSelf.request(method: method, path: path, parameters: parameters, encoding: encoding)
                    }
                }
                
                throw error
            }
    }
    
    override func headers() -> HTTPHeaders {
        var headers = HTTPHeaders()
        
        if let token = self.tokenProvider.getToken() {
            headers["Authorization"] = "Bearer \(token)"
        }
        
        return headers
    }
    
    private class EkscesoAuthURLBuilder: URLBuilder {
        
        static func url(forPath path: String) -> String {
            return ConfigUtils.AuthUrl + path
        }
        
    }
    
    private class EkscesoApiURLBuilder: URLBuilder {
        
        static func url(forPath path: String) -> String {
            return ConfigUtils.ApiUrl + path
        }
        
    }
}
