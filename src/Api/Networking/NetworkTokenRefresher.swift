//
//  NetworkTokenRefresher.swift
//  Api
//
//  Created by Patryk Mieszała on 28/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxSwift

public protocol NetworkTokenRefresher {
    
    func refreshToken() -> Single<Void>
}
