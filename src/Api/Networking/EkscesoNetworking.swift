//
//  EkscesoNetworking.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 17.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxMVVM
import Alamofire
import RxSwift

protocol EkscesoNetworking: Networking {
    
    func request<T>(method: HTTPMethod, path: String, parameters: [String : Any]?, encoding: ParameterEncoding, headersProvider: HeadersProvider) -> Single<T> where T : Decodable
}
