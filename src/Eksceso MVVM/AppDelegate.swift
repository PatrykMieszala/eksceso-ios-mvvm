//
//  AppDelegate.swift
//  Eksceso MVVM
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxMVVM

import UI
import Shared
import User
import Domain
import Api

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var mainCoordinator: MainCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        RxMVVMSettings.logEnabled = true
        RxMVVMSettings.logger = { entry in
            if entry.logLevel > .verbose {
                log.debug(entry.value, terminator: entry.terminator, file: entry.file, function: entry.function, line: entry.line)
            } else {
                log.verbose(entry.value, terminator: entry.terminator, file: entry.file, function: entry.function, line: entry.line)
            }
        }
        
        UIBarButtonItem
            .appearance()
            .setTitleTextAttributes(
                [
                    NSAttributedString.Key.font: Configuration.Fonts.barButtonTitle.with(size: 17)
                ],
                for: .normal
        )
        
        UIBarButtonItem
            .appearance()
            .setTitleTextAttributes(
                [
                    NSAttributedString.Key.font: Configuration.Fonts.barButtonTitle.with(size: 17)
                ],
                for: .highlighted
        )
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        let userFactory = UserFactory()
        let apiFactory = ApiFactory(tokenProvider: userFactory.getUserProvider(), tokenRefresher: userFactory.getUserProvider())
        let viewModelFactory = DomainFactory(userFactory: userFactory, apiFactory: apiFactory)
        
        self.mainCoordinator = MainCoordinator(window: window, userFactory: userFactory, viewModelFactory: viewModelFactory)
        self.mainCoordinator?.start()
        self.window = window
        
        return true
    }
}
