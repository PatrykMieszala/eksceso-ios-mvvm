//
//  EkscesoViewModel.swift
//  Domain
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxSwift
import User

public class EkscesoViewModel: ViewModel {
    
    public var isLightMode: Bool {
        return userProvider.colorMode.isLight
    }
    
    let userProvider: UserManaging
    let disposeBag: DisposeBag
    
    init(userProvider: UserManaging, disposeBag: DisposeBag) {
        self.userProvider = userProvider
        self.disposeBag = disposeBag
    }
    
    public func viewDidLoad() { }
    public func viewWillAppear() { }
    public func viewWillDisappear() { }
    public func viewDidAppear() { }
    public func viewDidDisappear() { }
}
