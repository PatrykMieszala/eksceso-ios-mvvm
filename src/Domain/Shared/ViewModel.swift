//
//  ViewModel.swift
//  Domain
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxSwift

public protocol ViewModel {
    
    var isLightMode: Bool { get }
    
    func viewDidLoad()
    func viewWillAppear()
    func viewWillDisappear()
    func viewDidAppear()
    func viewDidDisappear()
    
}
