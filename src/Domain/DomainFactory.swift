//
//  DomainFactory.swift
//  Domain
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxSwift
import User
import Api

public struct DomainFactory {
    
    let userFactory: UserFactory
    let apiFactory: ApiFactory
    
    public init(
        userFactory: UserFactory,
        apiFactory: ApiFactory
        ) {
        self.userFactory = userFactory
        self.apiFactory = apiFactory
    }
    
    public func getViewModel(disposeBag: DisposeBag) -> EkscesoViewModel {
        return EkscesoViewModel(
            userProvider: userFactory.getUserProvider(),
            disposeBag: disposeBag
        )
    }
    
    public func getSplashViewModel(onDoneCallback: @escaping SplashViewModel.DoneCallback, disposeBag: DisposeBag) -> SplashViewModel {
        return SplashViewModel(
            userProvider: userFactory.getUserProvider(),
            onDoneCallback: onDoneCallback,
            disposeBag: disposeBag
        )
    }
    
    public func getLoginWebViewModel(onDoneCallback: @escaping LoginWebViewModel.DoneCallback, disposeBag: DisposeBag) -> LoginWebViewModel {
        return LoginWebViewModel(
            userProvider: userFactory.getUserProvider(),
            onDoneCallback: onDoneCallback,
            disposeBag: disposeBag
        )
    }
    
    public func getAccountsViewModel(onDoneCallback: @escaping AccountsViewModel.DoneCallback, disposeBag: DisposeBag) -> AccountsViewModel {
        return AccountsViewModel(
            api: apiFactory.getAccountsApi(),
            userProvider: userFactory.getUserProvider(),
            onDoneCallback: onDoneCallback,
            disposeBag: disposeBag
        )
    }
    
    public func getDashboardViewModel(goToSettingsCallback: @escaping DashboardViewModel.GoToSettingsCallback, disposeBag: DisposeBag) -> DashboardViewModel {
        return DashboardViewModel(
            service: DashboardService(
                api: apiFactory.getDashboardApi(),
                userProvider: userFactory.getUserProvider()
            ),
            accountsApi: apiFactory.getAccountsApi(),
            userProvider: userFactory.getUserProvider(),
            csvExporter: CSVExportHelper(),
            goToSettings: goToSettingsCallback,
            disposeBag: disposeBag
        )
    }
    
    public func getDashboardSettingsViewModel(accountModel: AccountsResponseModel, disposeBag: DisposeBag) -> DashboardSettingsViewModel {
        return DashboardSettingsViewModel(userProvider: userFactory.getUserProvider(), accountModel: accountModel, disposeBag: disposeBag)
    }
}
