//
//  DashboardSortType.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 20/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public enum DashboardSortType: Int {
    case byDay = 0
    case byProject
    
    public var text: String {
        switch self {
        case .byDay: return "Day"
        case .byProject: return "Project"
        }
    }
}
