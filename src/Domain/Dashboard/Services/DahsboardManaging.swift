//
//  DahsboardManaging.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 17.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift

import Api

public protocol DashboardManaging {
    
    func getTimeEntries(from: Date?, to: Date?) -> Single<[TimeEntriesResponseModel]>
}
