//
//  DashboardService.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 17.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxMVVM

import Api
import User

final class DashboardService: DashboardManaging {
    
    private let api: DashboardApi
    private let userProvider: UserManaging
    
    // - MARK: Constructor(s)
    init(
        api: DashboardApi,
        userProvider: UserManaging
        ) {
        self.api = api
        self.userProvider = userProvider
    }
    
    // - MARK: Public Method(s)
    func getTimeEntries(from: Date?, to: Date?) -> Single<[TimeEntriesResponseModel]> {        
        let requests: [Observable<[TimeEntriesResponseModel]>] = userProvider
            .selectedAccounts
            .compactMap { [unowned self] account in
                return self.get(accountId: account.id, from: from, to: to).asObservable()
            }
        
        
        return Observable
            .zip(requests)
            .map { $0.joined().compactMap { $0 } }
            .asSingle()
            .timeout(10, scheduler: MainScheduler.asyncInstance)
    }
    
    // - MARK: Private Method(s)
    private func get(accountId: Int, from: Date?, to: Date?, page: Int? = nil, responses: [TimeEntriesResponseModel] = []) -> Single<[TimeEntriesResponseModel]> {
        return api
            .get(accountId: accountId, projectId: nil, from: from, to: to, page: page, perPage: nil)
            .flatMap { [weak self] response in
                guard let `self` = self, let nextPage = response.nextPage else {
                    return .just(responses + [response])
                }
                
                return self.get(accountId: accountId, from: from, to: to, page: nextPage, responses: responses + [response])
        }
    }
}
