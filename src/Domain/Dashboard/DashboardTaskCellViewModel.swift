//
//  DashboardTaskCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

import Api

public class DashboardTaskCellViewModel: Equatable, Comparable, IdentifiableType {
    
    enum State {
        case empty
        case loading
    }
    
    public typealias Identity = String
    
    public var identity: String { return String(id) + String(describing: isLast) }
    
    public let id: Int
    public let projectId: Int
    
    public let clientName: String
    public let projectName: String
    public let taskType: String
    public let taskName: String?
    public let hours: Double
    public let time: String
    public let color: UIColor?
    
    public let spentDate: Date?
    public let spentDateString: String?
    
    let timeEntryProject: TimeEntry.TimeEntryProject
    
    var isLast: Bool = false
    private(set) public var isDateHidden: Bool = false
    
    public let isLoading: Bool
    
    init(model: TimeEntry, color: UIColor?) {
        self.id = model.id
        self.projectId = model.project.id
        self.color = color
        
        self.spentDate = model.dateSpentDate
        self.spentDateString = model.dateSpentDate?.toString(.custom("d MMMM, yyyy"))
        
        self.clientName = model.client.name
        self.projectName = model.project.name
        self.taskType = model.task.name
        self.taskName = model.notes
        self.timeEntryProject = model.project
        self.hours = model.hours
        
        let hour = Int(model.hours)
        let minute = 60 * (model.hours - model.hours.rounded(.down))
        
        let nf = NumberFormatter()
        nf.minimumIntegerDigits = 2
        nf.maximumFractionDigits = 0
        
        self.time = "\(nf.string(for: hour) ?? "00"):\(nf.string(for: minute) ?? "00")"
        
        self.isLoading = false
    }
    
    init(state: State) {        
        self.id = UUID().hashValue
        self.projectId = -1
        self.color = .white
        
        self.spentDate = nil
        self.spentDateString = nil
        
        switch state {
        case .empty:
            self.clientName = "Ouuups"
            self.projectName = "Looks like your chosen interval is empty"
            self.taskType = ""
            self.taskName = ""
            self.isLoading = false
        case .loading:
            self.clientName = "             "
            self.projectName = "                                    "
            self.taskType = "               "
            self.taskName = "                                                   "
            self.isLoading = true
        }
        
        self.timeEntryProject = TimeEntry.TimeEntryProject(id: -1, name: "", code: "")
        
        self.hours = 0
        self.time = "           "
    }
    
    func setDate(hidden: Bool) {
        self.isDateHidden = hidden
    }
}

public func ==(lhs: DashboardTaskCellViewModel, rhs: DashboardTaskCellViewModel) -> Bool {
    return lhs.identity == rhs.identity
}

public func < (lhs: DashboardTaskCellViewModel, rhs: DashboardTaskCellViewModel) -> Bool {
    return (lhs.spentDate?.timeIntervalSince1970 ?? 0) < (rhs.spentDate?.timeIntervalSince1970 ?? 0)
}
