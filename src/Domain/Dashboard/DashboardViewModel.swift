//
//  DashboardViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import RxMVVM
import RxDataSources

import Charts
import SwiftDate
import DynamicColor

import Api
import User
import Shared

public class DashboardViewModel: EkscesoViewModel, RxViewModel {
    
    public typealias GoToSettingsCallback = (AccountsResponseModel) -> ()
    
    public typealias Feed = [FeedSection]
    public typealias FeedSection = AnimatableSectionModel<String, DashboardTaskCellViewModel>
    
    public lazy var refreshCommand: RxCommand<Void, Void> = {
        return command(method: DashboardViewModel.refresh)
    }()
    
    public lazy var shareCSVCommand: RxCommand<Void, URL> = {
        return command(method: DashboardViewModel.shareCSV)
    }()
    
    public lazy var changeSortCommand: RxCommand<DashboardSortType, Void> = {
        return command(method: DashboardViewModel.changeSort)
    }()
    
    public lazy var filterByChartEntryCommand: RxCommand<DashboardChartEntry?, Void> = {
        return command(method: DashboardViewModel.filterBy)
    }()
    
    lazy var getEntriesCommand: RxCommand<IntervalOption, [TimeEntriesResponseModel]> = {
        return command(method: DashboardViewModel.getTimeEntries)
    }()
    
    lazy var getAccountsCommand: RxCommand<Void, AccountsResponseModel> = {
        return command(method: DashboardViewModel.getAccounts)
    }()
    
    public let intervalText: Driver<String>
    public let chartEntries: Driver<[DashboardChartEntry]>
    public let tableFeed: Driver<Feed>
    public let maxHours: Driver<String>
    public let hoursAmount: Driver<String>
    public let hoursColor: Driver<UIColor>
    
    let intervalTextBehavior: BehaviorRelay<String> = .init(value: "")
    let chartEntriesBehavior: BehaviorRelay<[DashboardChartEntry]> = .init(value: [])
    let tableFeedBehavior: BehaviorRelay<Feed> = .init(value: [])
    let maxHoursBehavior: BehaviorRelay<String> = .init(value: "")
    let hoursAmountBehavior: BehaviorRelay<String> = .init(value: "")
    let hoursColorBehavior: BehaviorRelay<UIColor> = .init(value: .white)
    
    var timeEntries: [TimeEntriesResponseModel] = []
    var sort: DashboardSortType = .byDay
    var filterProjectId: Int?
    
    var accountsResponse: AccountsResponseModel?
    
    let service: DashboardManaging
    let accountsApi: AccountsApi
    let csvExporter: CSVExporter
    let goToSettings: GoToSettingsCallback
    
    var interval: IntervalOption {
        return userProvider.interval
    }
    
    init(
        service: DashboardManaging,
        accountsApi: AccountsApi,
        userProvider: UserManaging,
        csvExporter: CSVExporter,
        goToSettings: @escaping GoToSettingsCallback,
        disposeBag: DisposeBag
        ) {
        self.service = service
        self.accountsApi = accountsApi
        self.csvExporter = csvExporter
        self.goToSettings = goToSettings
        
        self.intervalText = self.intervalTextBehavior.asDriver()
        self.chartEntries = self.chartEntriesBehavior.asDriver()
        self.tableFeed = self.tableFeedBehavior.asDriver()
        self.maxHours = self.maxHoursBehavior.asDriver()
        self.hoursAmount = self.hoursAmountBehavior.asDriver()
        self.hoursColor = self.hoursColorBehavior.asDriver()
        
        super.init(userProvider: userProvider, disposeBag: disposeBag)
    }
    
    public override func viewWillAppear() {
        super.viewWillAppear()
        
        self.refreshCommand.execute()
    }
    
    public func didTapSettings() {
        guard let accountsResponse = accountsResponse else { return }
        self.goToSettings(accountsResponse)
    }
    
    //MARK: - Commands
    private func refresh() -> Single<Void> {
        return Single
            .create { [unowned self] (event) -> Disposable in
                return self.refreshData(event: event)
        }
    }
    
    private func refreshData(event: (@escaping (SingleEvent<Void>) -> ())) -> Disposable {
        let interval: IntervalOption = self.interval
        
        let intervalText: String = DashboardViewModel.getText(fromInterval: interval)
        self.intervalTextBehavior.accept(intervalText)
        
        self.setEmptys()
        
        let commands: Single<Void> = Single
            .zip(
                self.getEntriesCommand.execute(interval, dispose: true),
                self.getAccountsCommand.execute(dispose: true)
            ) { [weak self] (timeEntries: [TimeEntriesResponseModel], accounts: AccountsResponseModel) in
                self?.consume(timeEntries: timeEntries, interval: interval, filterByProjectId: nil)
                self?.consume(accounts: accounts)
                
                event(.success(()))
            }
            .doOnError { (error: Error) in
                event(.error(error))
            }
            .map { _ in return }
        
        return commands.subscribe()
    }
    
    private func getTimeEntries(interval: IntervalOption) -> Single<[TimeEntriesResponseModel]> {
        return service.getTimeEntries(from: interval.from, to: interval.to)
    }
    
    private func getAccounts() -> Single<AccountsResponseModel> {
        return accountsApi.get()
    }
    
    private func shareCSV() -> Single<URL> {
        return Single
            .create(subscribe: { [unowned self] (event) -> Disposable in
                do {
                    let entries = self.timeEntries
                        .map { $0.timeEntries }
                        .joined()
                        .compactMap { $0 }
                    
                    let csvURL = try self.csvExporter.createCSVFile(fromTimeEntries: entries)
                    
                    event(.success(csvURL))
                }
                catch let error {
                    event(.error(error))
                }
                
                return Disposables.create()
            })
    }
    
    private func changeSort(sort: DashboardSortType) -> Single<Void> {
        self.sort = sort
        let viewModels = DashboardViewModel.getCellViewModels(fromResponse: self.timeEntries, filterByProjectId: self.filterProjectId)
        let feed = DashboardViewModel.getFeed(fromViewModels: viewModels, sort: sort)

        self.tableFeedBehavior.accept(feed)
        
        return .just(())
    }
    
    private func filterBy(chartEntry: DashboardChartEntry?) -> Single<Void> {
        let projectId = chartEntry?.project?.id
        self.filterProjectId = projectId
        
        let viewModels = DashboardViewModel.getCellViewModels(fromResponse: self.timeEntries, filterByProjectId: projectId)
        let feed = DashboardViewModel.getFeed(fromViewModels: viewModels, sort: self.sort)
        self.tableFeedBehavior.accept(feed)
        
        let hoursAmount = DashboardViewModel.getHoursAmount(fromViewModels: viewModels)
        let hoursAmountString = DashboardViewModel.getHoursAmountString(fromValue: hoursAmount)
        self.hoursAmountBehavior.accept(hoursAmountString)
        
        let maxHours = DashboardViewModel.getMaxHours(fromInterval: interval, viewModels: viewModels, userProvider: userProvider)
        let maxHoursString = DashboardViewModel.getMaxHoursString(fromValue: maxHours)
        self.maxHoursBehavior.accept(maxHoursString)
        
        let hoursColor = DashboardViewModel.getHoursColor(fromHoursAmount: hoursAmount, maxHours: maxHours)
        self.hoursColorBehavior.accept(hoursColor)
        
        return .just(())
    }
    
    //MARK: - Data handling
    private func consume(
        timeEntries: [TimeEntriesResponseModel],
        interval: IntervalOption,
        filterByProjectId projectId: Int?
        ) {
        self.timeEntries = timeEntries
        
        let viewModels = DashboardViewModel.getCellViewModels(fromResponse: timeEntries, filterByProjectId: projectId)
        let feed = DashboardViewModel.getFeed(fromViewModels: viewModels, sort: self.sort)
        self.tableFeedBehavior.accept(feed)
        
        let chartEntries = DashboardViewModel.getChartEntries(fromViewModels: viewModels)
        self.chartEntriesBehavior.accept(chartEntries)
        
        let hoursAmount = DashboardViewModel.getHoursAmount(fromViewModels: viewModels)
        let hoursAmountString = DashboardViewModel.getHoursAmountString(fromValue: hoursAmount)
        self.hoursAmountBehavior.accept(hoursAmountString)
        
        let maxHours = DashboardViewModel.getMaxHours(fromInterval: interval, viewModels: viewModels, userProvider: userProvider)
        let maxHoursString = DashboardViewModel.getMaxHoursString(fromValue: maxHours)
        self.maxHoursBehavior.accept(maxHoursString)
        
        let hoursColor = DashboardViewModel.getHoursColor(fromHoursAmount: hoursAmount, maxHours: maxHours)
        self.hoursColorBehavior.accept(hoursColor)
    }
    
    private func consume(accounts: AccountsResponseModel) {
        self.accountsResponse = accounts
    }
    
    //MARK: - Helpers
    private func setEmptys() {
        self.hoursAmountBehavior.accept("--:--")
        self.hoursColorBehavior.accept(Configuration.Colors.brand.color)
        
        let chartTemplates: [DashboardChartEntry] = Array(repeatElement(0, count: 3))
            .map { _ in DashboardChartEntry(value: Double.pi) }
        self.chartEntriesBehavior.accept(chartTemplates)
        
        let tableTemplates: [DashboardTaskCellViewModel] = Array(repeatElement(0, count: 3))
            .map { _ in DashboardTaskCellViewModel(state: .loading) }
        let tableTemplateFeed: Feed = [FeedSection(model: "", items: tableTemplates)]
        
        self.tableFeedBehavior.accept(tableTemplateFeed)
    }
    
    private class func getText(fromInterval interval: IntervalOption) -> String {
        if case .customMonth = interval {
            return interval.from.monthName(SymbolFormatStyle.default)
        }
        
        return interval.text
    }
    
    private class func getCellViewModels(
        fromResponse timeEntries: [TimeEntriesResponseModel],
        filterByProjectId projectId: Int?
        ) -> [DashboardTaskCellViewModel] {
        
        let projects: [String] = timeEntries
            .map { (model: TimeEntriesResponseModel) in
                return model
                    .timeEntries
                    .reduce(into: []) { (list, entry) in
                        if list.contains(entry.project.name) == false {
                            list.append(entry.project.name)
                        }
                }
            }
            .joined()
            .compactMap { $0 }
        
        let colors = ChartColorTemplates.colorful() + ChartColorTemplates.material() + ChartColorTemplates.pastel() + ChartColorTemplates.joyful() + ChartColorTemplates.liberty()
        
        let projectWithColors: [String: UIColor] = projects
            .enumerated()
            .reduce(into: [:]) { (dict, enumeration) in
                if colors.count > enumeration.offset {
                    dict[enumeration.element] = colors[enumeration.offset]
                }
                else {
                    //TODO: properly handle colors out of index!
                    dict[enumeration.element] = colors.last
                }
        }
        
        let vms: [DashboardTaskCellViewModel] = timeEntries
            .map { (model: TimeEntriesResponseModel) -> [DashboardTaskCellViewModel] in
                return model
                    .timeEntries
                    .map { DashboardTaskCellViewModel(model: $0, color: projectWithColors[$0.project.name]) }
            }
            .joined()
            .compactMap { $0 }
        
        if let projectId = projectId {
            return vms
                .filter { $0.projectId == projectId }
        }
        
        return vms
    }
    
    private class func getFeed(fromViewModels viewModels: [DashboardTaskCellViewModel], sort: DashboardSortType) -> Feed {
        
        switch sort {
        case .byDay:
            return viewModels
                .reduce(into: Feed()) { (sections: inout Feed, vm: DashboardTaskCellViewModel) in
                    vm.setDate(hidden: true)
                    
                    if let index = sections.index(where: { (section: FeedSection) -> Bool in
                        section.model == vm.spentDateString
                    }) {
                        var section = sections.remove(at: index)
                        section.items.append(vm)
                        section.items.forEach { $0.isLast = false }
                        section.items.last?.isLast = true
                        
                        sections.insert(section, at: index)
                    }
                    else {
                        let section = FeedSection(model: vm.spentDateString ?? "", items: [vm])
                        section.items.last?.isLast = true
                        
                        sections.append(section)
                    }
                }
                .sorted { left, right in
                    (left.items.first?.spentDate?.timeIntervalSince1970 ?? 0) > (right.items.first?.spentDate?.timeIntervalSince1970 ?? 0)
            }
            
        case .byProject:
            return viewModels
                .reduce(into: Feed()) { (sections: inout Feed, vm: DashboardTaskCellViewModel) in
                    vm.setDate(hidden: false)
                    
                    if let index = sections.index(where: { (section: FeedSection) -> Bool in
                        section.model == vm.projectName
                    }) {
                        var section = sections.remove(at: index)
                        section.items.append(vm)
                        section.items.forEach { $0.isLast = false }
                        section.items.last?.isLast = true
                        section.items.sort()
                        
                        sections.insert(section, at: index)
                    }
                    else {
                        var section = FeedSection(model: vm.projectName, items: [vm])
                        section.items.last?.isLast = true
                        section.items.sort()
                        
                        sections.append(section)
                    }
                }
                .sorted { left, right in
                    (left.model) < (right.model)
            }
        }
    }
    
    private class func getChartEntries(fromViewModels viewModels: [DashboardTaskCellViewModel]) -> [DashboardChartEntry] {
        
        let entriesPerProject: [String: [DashboardTaskCellViewModel]] = viewModels
            .reduce(into: [:]) { (dict, vm) in
                if var list = dict[vm.projectName] {
                    list.append(vm)
                    dict[vm.projectName] = list
                }
                else {
                    dict[vm.projectName] = [vm]
                }
        }
        
        let chartEntries: [DashboardChartEntry] = entriesPerProject
            .reduce([]) { (list, project) in
                let entries = project.value
                
                let hours: Double = entries
                    .reduce(0) { (value, entry) in
                        return value + entry.hours
                }
                
                guard let first = project.value.first else { return list }
                
                let entry = DashboardChartEntry(timeEntryProject: first.timeEntryProject, hours: hours, color: first.color)
                
                return list + [entry]
            }
            .sorted { (left, right) -> Bool in
                return left.label ?? "" < right.label ?? ""
        }
        
        return chartEntries
    }
    
    private class func getHoursAmount(fromViewModels viewModels: [DashboardTaskCellViewModel]) -> Double {
        let hoursAmount: Double = viewModels
            .reduce(0) { $0 + $1.hours }
        
        return hoursAmount
    }
    
    private class func getHoursAmountString(fromValue hours: Double) -> String {
        let hour = Int(hours)
        let minute = 60 * (hours - hours.rounded(.down))
        
        let nf = NumberFormatter()
        nf.minimumIntegerDigits = 2
        nf.maximumFractionDigits = 0
        
        return "\(nf.string(for: hour) ?? "00"):\(nf.string(for: minute) ?? "00")"
    }
    
    private class func getMaxHours(
        fromInterval interval: IntervalOption,
        viewModels: [DashboardTaskCellViewModel],
        userProvider: UserManaging
        ) -> Double {
        
        let from: Date = interval.from
        let to: Date = interval.to
        
        let components = Foundation.Calendar.current.dateComponents([.hour], from: to)
        var workingDays: [Date] = []
        
        Foundation
            .Calendar
            .current
            .enumerateDates(
                startingAfter: from,
                matching: components,
                matchingPolicy: .nextTimePreservingSmallerComponents,
                using: { (date, _, stop) in
                    guard let date = date else {
                        stop = true
                        
                        return
                    }
                    
                    if date.isInWeekend == false,
                        userProvider.isHoliday(forDate: date) == false
                    {
                        workingDays.append(date)
                    }
                    
                    if date.isAfterDate(to, orEqual: true, granularity: .day) {
                        stop = true
                    }
            })
        
        let hours: Double = Double(workingDays.count * 8)
        
        return hours
        
        //TODO: uncomment when implementing filter feature
        //let maxHoursTotal: Double = viewModels.reduce(0) { $0 + $1.hours }
        //let filtered = selected != nil
        //let hours = filtered ? maxHoursTotal : maxHours
        
        //let prefix: String = filtered ? "\nfrom " : "\nmax "
    }
    
    private class func getMaxHoursString(fromValue hours: Double) -> String {
        let prefix: String = "\nmax "
        
        let hour = Int(hours)
        let minute = 60 * (hours - hours.rounded(.down))
        
        let nf = NumberFormatter()
        nf.minimumIntegerDigits = 2
        nf.maximumFractionDigits = 0
        
        return prefix + "\(nf.string(for: hour) ?? "00"):\(nf.string(for: minute) ?? "00")"
    }
    
    private class func getHoursColor(fromHoursAmount hours: Double, maxHours: Double) -> UIColor {
        let flatGreen = Configuration.Colors.flatGreen.color
        
        let diff: CGFloat = max(0, CGFloat(hours) - CGFloat(maxHours))
        let weight: CGFloat = (diff / 3)
        
        let color = flatGreen.mixed(withColor: Configuration.Colors.flatRed.color, weight: weight, inColorSpace: .rgb)
        
        return color
    }
    
    public func sectionTitle(forSection section: Int) -> String? {
        let feed = self.tableFeedBehavior.value
        
        guard feed.count > section else { return nil }
        
        let section = feed[section]
        
        return section.model
    }

    public func sectionHoursString(forSection section: Int) -> String? {
        let feed = self.tableFeedBehavior.value
        
        guard feed.count > section else { return nil }
        
        let items = feed[section].items
        let hoursSum: Double = items.reduce(0) { (result, item) -> Double in
            return result + item.hours
        }
        
        let hours = Int(hoursSum)
        let minute = 60 * (hoursSum - hoursSum.rounded(.down))
        
        let nf = NumberFormatter()
        nf.minimumIntegerDigits = 2
        nf.maximumFractionDigits = 0
        
        return "\(nf.string(for: hours) ?? "00"):\(nf.string(for: minute) ?? "00")"
    }
}
