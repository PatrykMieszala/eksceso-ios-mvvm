//
//  DashboardChartEntry.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 11.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import Charts

import Api

public class DashboardChartEntry: PieChartDataEntry {
    
    private(set) var project: TimeEntry.TimeEntryProject?
    private(set) public var color: UIColor?
    
    convenience init(timeEntryProject project: TimeEntry.TimeEntryProject?, hours: Double, color: UIColor?) {
        self.init(value: hours, label: project?.name, icon: nil, data: nil)
        self.project = project
        self.color = color
    }
}
