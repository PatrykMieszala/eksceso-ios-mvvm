//
//  DashboardSettingsAccountCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 13.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMVVM

import Api
import User

public class DashboardSettingsAccountCellViewModel: DashboardSettingsCellViewModel {
    
    public var title: String {
        return model.name
    }
    
    override public var isSelectedValue: Bool {
        return userProvider.selectedAccounts.contains(model)
    }
    
    override public var titleDriver: Driver<String?> {
        return .just(title)
    }
    
    private let model: AccountModel
    private let userProvider: UserManaging
    
    init(model: AccountModel, userProvider: UserManaging, isLast: Bool) {
        self.model = model
        self.userProvider = userProvider
        
        super.init(isLast: isLast)
    }
    
    override public func set(selected: Bool) {
        guard isBlocked == false else { return }
        selected ? userProvider.select(account: model) : userProvider.unselect(account: model)
    }
}
