//
//  DashboardSettingsSectionType.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 21/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxDataSources

public enum DashboardSettingsSectionType: Int, IdentifiableType {
    
    public typealias Identity = Int
    
    case accounts = 0
    case interval
    case daysOff
    case colorMode
    
    public var identity: Int {
        return self.rawValue
    }
    
    public var title: String {
        switch self {
        case .accounts: return "Accounts"
        case .interval: return "Interval"
        case .daysOff: return "Days off"
        case .colorMode: return "Color mode"
        }
    }
}
