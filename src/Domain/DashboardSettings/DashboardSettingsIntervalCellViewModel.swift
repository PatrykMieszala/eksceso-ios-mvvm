//
//  DashboardSettingsIntervalCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMVVM

import User
import Shared

public class DashboardSettingsIntervalCellViewModel: DashboardSettingsCellViewModel {
    
    // - MARK: Public Variable(s)
    var title: String {
        return interval.text
    }
    
    override public var titleDriver: Driver<String?> {
        return .just(title)
    }
    
    override public var isSelectedValue: Bool {
        return userProvider.interval == interval
    }
    
    override public var isCustomDateViewHidden: Driver<Bool> {
        return isSelected
            .map { [unowned self] selected in
                guard selected else { return true }
                if case .customMonth = self.interval {
                    return false
                }
                
                return true
        }
    }
    
    public var isCustomInterval: Bool {
        if case .customMonth = self.interval {
            return true
        }
        
        return false
    }
    
    public var customMonth: Driver<Date> {
        return customMonthBehavior.asDriver()
    }
    
    // - MARK: Private Variable(s)
    private let userProvider: UserManaging
    private var interval: IntervalOption
    
    private let customMonthBehavior: BehaviorRelay<Date>
    
    // - MARK: Constructor(s)
    init(interval: IntervalOption, userProvider: UserManaging, isLast: Bool) {
        self.userProvider = userProvider
        self.interval = interval
        
        self.customMonthBehavior = BehaviorRelay(value: interval.from)
        
        super.init(isLast: isLast)
    }
    
    // - MARK: Public Method(s)
    override public func set(selected: Bool) {
        guard isBlocked == false else { return }
        
        if case .customMonth = self.interval {
            super.set(selected: selected)
        }
        
        guard selected else { return }
        userProvider.set(interval: interval)
    }
    
    public func bind(customMonth: Driver<Date>, disposeBag: DisposeBag) {
        guard case .customMonth = interval else { return }
        
        customMonth
            .drive(customMonthBehavior)
            .dispose(in: disposeBag)
        
        customMonth
            .driveNext { [unowned self] customMonth in
                let interval = IntervalOption.customMonth(customMonth)
                
                self.interval = interval
                self.userProvider.set(interval: interval)
            }
            .dispose(in: disposeBag)
    }
    
    // - MARK: Private Method(s)
}
