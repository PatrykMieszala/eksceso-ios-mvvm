//
//  DashboardSettingsColorModeCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 21/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMVVM

import User
import Shared

public class DashboardSettingsColorModeCellViewModel: DashboardSettingsCellViewModel {
    
    // - MARK: Public Variable(s)
    public var title: String {
        return colorMode.text
    }
    
    override public var titleDriver: Driver<String?> {
        return .just(title)
    }
    
    override public var isSelectedValue: Bool {
        return userProvider.colorMode == colorMode
    }
    
    // - MARK: Private Variable(s)
    private let userProvider: UserManaging
    private let colorMode: ColorMode
    
    // - MARK: Constructor(s)
    init(colorMode: ColorMode, userProvider: UserManaging, isLast: Bool) {
        self.userProvider = userProvider
        self.colorMode = colorMode
        
        super.init(isLast: isLast)
    }
    
    // - MARK: Public Method(s)
    override public func set(selected: Bool) {
        guard selected, isBlocked == false else { return }
        userProvider.set(colorMode: colorMode)
    }
    
    // - MARK: Private Method(s)
}
