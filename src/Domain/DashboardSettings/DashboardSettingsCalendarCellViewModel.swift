//
//  DashboardSettingsCalendarCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxMVVM

import User

public class DashboardSettingsCalendarCellViewModel: DashboardSettingsCellViewModel {
    
    // - MARK: Public Variable(s)
    public var minDate: Date {
        return calendar.date(byAdding: .year, value: -1, to: Date()) ?? Date()
    }
    
    public var maxDate: Date {
        return calendar.date(byAdding: .year, value: 1, to: Date()) ?? Date()
    }
    
    public var selectedDates: [Date] {
        return userProvider.daysOff
    }
    
    // - MARK: Private Variable(s)
    private let calendar: Calendar = Calendar.current
    private let userProvider: UserManaging
    
    // - MARK: Constructor(s)
    init(
        userProvider: UserManaging
        ) {
        self.userProvider = userProvider
        
        super.init(isLast: true)
    }
    
    // - MARK: Public Method(s)
    public func didSelect(date: Date) {
        userProvider.add(dayOff: date)
    }
    
    public func didDeselect(date: Date) {
        userProvider.remove(dayOff: date)
    }
    
    // - MARK: Private Method(s)
}
