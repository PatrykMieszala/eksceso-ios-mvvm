//
//  DashboardSettingsCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

public class DashboardSettingsCellViewModel: Equatable, IdentifiableType {
    
    public typealias Identity = String
    
    public var isSelected: Driver<Bool> {
        return selectedBehavior.asDriver()
    }
    
    public var isSelectedValue: Bool {
        return selectedBehavior.value
    }
    
    public var titleDriver: Driver<String?> {
        return .empty()
    }
    
    public var isCustomDateViewHidden: Driver<Bool> {
        return .just(true)
    }
    
    public let isLast: Bool
    private(set) var isBlocked: Bool = false
    
    public let identity: String = NSUUID().uuidString
    private let selectedBehavior: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    init(isLast: Bool) {
        self.isLast = isLast
    }
    
    public func set(selected: Bool) {
        selectedBehavior.accept(selected)
    }
    
    public func unblock() {
        self.isBlocked = false
    }
    
    public func blockChanges() {
        self.isBlocked = true
    }
}

public func ==(lhs: DashboardSettingsCellViewModel, rhs: DashboardSettingsCellViewModel) -> Bool {
    return lhs.identity == rhs.identity
}
