//
//  DashboardSettingsViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 08.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMVVM
import RxOptional
import RxDataSources

import Api
import User
import Shared

public class DashboardSettingsViewModel: EkscesoViewModel {
    
    public typealias Feed = [FeedSection]
    public typealias FeedSection = AnimatableSectionModel<DashboardSettingsSectionType, DashboardSettingsCellViewModel>
    
    public let feed: Driver<Feed>
    public private(set) var initialSelectedIndexes: [IndexPath] = []
    
    let feedBehavior: BehaviorRelay<Feed> = .init(value: [])
    
    init(
        userProvider: UserManaging,
        accountModel: AccountsResponseModel,
        disposeBag: DisposeBag
        ) {
        self.feed = self.feedBehavior.asDriver()
        
        super.init(userProvider: userProvider, disposeBag: disposeBag)
        
        let currentInterval = userProvider.interval
        
        let allIntervals = IntervalOption.allCasesWhenCustom(month: currentInterval.from)
        
        let intervals = allIntervals
            .enumerated()
            .map { DashboardSettingsIntervalCellViewModel(interval: $0.element, userProvider: userProvider, isLast: $0.offset == allIntervals.count - 1 ) }
        
        var accountsSection: FeedSection {
            let accounts = accountModel.accounts
            
            let items = accounts
                .sorted { $0.name < $1.name }
                .enumerated()
                .map { DashboardSettingsAccountCellViewModel(model: $0.element, userProvider: userProvider, isLast: $0.offset == accounts.count - 1) }
            
            return FeedSection(model: .accounts, items: items)
        }
        var intervalSection: FeedSection {
            return FeedSection(model: .interval, items: intervals)
        }
        var daysOffSection: FeedSection {
            return FeedSection(model: .daysOff, items: [DashboardSettingsCalendarCellViewModel(userProvider: userProvider)])
        }
        
        let colorModes = ColorMode
            .allCases
            .enumerated()
            .map { DashboardSettingsColorModeCellViewModel(colorMode: $0.element, userProvider: userProvider, isLast: $0.offset == allIntervals.count - 1 ) }
        
        var colorModeSection: FeedSection {
            return FeedSection(model: .colorMode, items: colorModes)
        }
        
        let feed: Feed = [accountsSection, intervalSection, daysOffSection, colorModeSection]
        
        self.feedBehavior.accept(feed)
        
        self.initialSelectedIndexes = feed
            .enumerated()
            .map { (sectionIndex, section) in
                return section
                    .items
                    .enumerated()
                    .reduce(into: []) { (indexPaths, enumeration) in
                        if enumeration.element.isSelectedValue {
                            let indexPath = IndexPath(item: enumeration.offset, section: sectionIndex)
                            indexPaths.append(indexPath)
                        }
                }
            }
            .joined()
            .compactMap { $0 }
    }
    
    public func didTapLogout() {
        self.userProvider.logout()
    }
}
