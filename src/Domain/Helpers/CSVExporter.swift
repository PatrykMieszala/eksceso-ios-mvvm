//
//  CSVExporter.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 20/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import Api

protocol CSVExporter {
    
    func createCSVFile(fromTimeEntries timeEntries: [TimeEntry]) throws -> URL
}
