//
//  CSVExportHelper.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 20/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import Api
import Shared

class CSVExportHelper: CSVExporter {
    
    private var fileManager: FileManager {
        return FileManager.default
    }
    
    private func documentsURL() throws -> URL {
        return try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    private func url(forFileName fileName: String) throws -> URL {
        return try documentsURL().appendingPathComponent(fileName).appendingPathExtension("csv")
    }
    
    private func template(username: String) -> String {
        let headerDateString = Date().toString(.custom("HH:mm, dd.MM.yyyy"))
        
        let stringFormat =
            "Eksceso" + "\n"
                + headerDateString + "\n\n"
                + "User: " + username + "\n\n"
                + "Date" + ";"
                + "Client" + ";"
                + "Project" + ";"
                + "Task" + ";"
                + "Hours" + ";"
                + "Billable" + ";"
                + "Notes" + ";"
        
        return stringFormat
    }
    
    func createCSVFile(fromTimeEntries timeEntries: [TimeEntry]) throws -> URL {
        
        let username: String = timeEntries.first?.user.name ?? ""
        var csv = template(username: username)
        
        timeEntries
            .forEach { (entry) in
                //Template: Date;Client;Project;Task;Hours;Billable;Notes;
                
                let date: String = entry.dateSpentDate?.toString(.custom("dd MMMM yyyy")) ?? ""
                let client: String = entry.client.name
                let project: String = entry.project.name
                let task: String = entry.task.name
                let hours: String = String(entry.hours)
                let billable: String = String(entry.billable)
                let notes: String = entry.notes ?? ""
                
                let csvLine: String = "\n"
                    + date + ";"
                    + client + ";"
                    + project + ";"
                    + task + ";"
                    + hours + ";"
                    + billable + ";"
                    + notes + ";"
                
                csv.append(contentsOf: csvLine)
        }
        
        log.debug(csv, terminator: "\n")
        
        let fileNameDateString = Date().toString(.custom("dd_MM_yyyy"))
        
        let fileName = String(format: "Eksceso_export_%@", fileNameDateString)
        let fileURL = try url(forFileName: fileName)
        
        try csv.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        
        return fileURL
    }
}
