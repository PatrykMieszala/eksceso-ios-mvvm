//
//  SplashViewModel.swift
//  Domain
//
//  Created by Patryk Mieszała on 27/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import RxSwift
import User

public class SplashViewModel: EkscesoViewModel {
    
    public typealias DoneCallback = ((AuthorizationResult) -> Void)
    
    let onDoneCallback: DoneCallback
    
    init(userProvider: UserManaging, onDoneCallback: @escaping DoneCallback, disposeBag: DisposeBag) {
        self.onDoneCallback = onDoneCallback
        
        super.init(userProvider: userProvider, disposeBag: disposeBag)
    }
    
    public override func viewDidLoad() {
        self.userProvider
            .authorize()
            .subscribeCompleted { [weak self] (result) in
                self?.onDoneCallback(result)
            }
            .dispose(in: disposeBag)
    }
}
