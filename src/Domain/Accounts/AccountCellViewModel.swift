//
//  AccountCellViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxMVVM

import Api

public class AccountCellViewModel: Equatable {
    
    // - MARK: Public Variable(s)
    public let name: String
    public let isLast: Bool
    
    let model: AccountModel
    
    // - MARK: Constructor(s)
    init(model: AccountModel, isLast: Bool) {
        self.model = model
        self.name = model.name
        self.isLast = isLast
    }
    
    // - MARK: Public Method(s)
    
    // - MARK: Private Method(s)
}

public func == (lhs: AccountCellViewModel, rhs: AccountCellViewModel) -> Bool {
    return lhs.model == rhs.model
}
