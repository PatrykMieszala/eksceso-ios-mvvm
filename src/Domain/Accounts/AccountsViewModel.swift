//
//  AccountsViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import RxMVVM

import Api
import User

public class AccountsViewModel: EkscesoViewModel {
    
    public typealias DoneCallback = (() -> ())
    
    public var feed: Driver<[AccountCellViewModel]> {
        return feedBehavior.asDriver()
    }
    
    public var isFeedEmpty: Driver<Bool> {
        return feed.map { $0.isEmpty }
    }
    
    public var isDoneButtonHidden: Signal<Bool> {
        return isDoneButtonHiddenPublish.asSignal()
    }
    
    let feedBehavior: BehaviorRelay<[AccountCellViewModel]> = .init(value: [])
    let isDoneButtonHiddenPublish: PublishRelay<Bool> = .init()
    
    var selectedEntries: [AccountCellViewModel] = [] {
        didSet {
            isDoneButtonHiddenPublish.accept(selectedEntries.isEmpty)
        }
    }
    
    let onDoneCallback: DoneCallback
    let api: AccountsApi
    
    init(api: AccountsApi, userProvider: UserManaging, onDoneCallback: @escaping DoneCallback, disposeBag: DisposeBag) {
        self.onDoneCallback = onDoneCallback
        self.api = api
        
        api
            .get()
            .map { response -> [AccountCellViewModel] in
                let count = response.accounts.count
                
                return response
                    .accounts
                    .enumerated()
                    .map { AccountCellViewModel(model: $0.element, isLast: $0.offset == count - 1)}
            }
            .asDriver(onErrorJustReturn: [])
            .drive(feedBehavior)
            .dispose(in: disposeBag)
        
        super.init(userProvider: userProvider, disposeBag: disposeBag)
    }
    
    public func didSelectRowAt(indexPath: IndexPath) {
        let viewModel = feedBehavior.value[indexPath.row]
        self.selectedEntries.append(viewModel)
        
        userProvider.select(account: viewModel.model)
    }
    
    public func didDeselectRowAt(indexPath: IndexPath) {
        let viewModel = feedBehavior.value[indexPath.row]
        self.selectedEntries.removeAll(where: { $0 == viewModel })
        
        userProvider.unselect(account: viewModel.model)
    }
    
    public func didTapDoneButton() {
        guard self.selectedEntries.isEmpty == false else { return }
        
        self.onDoneCallback()
    }
}

