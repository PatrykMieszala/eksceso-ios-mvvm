//
//  LoginWebViewModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import WebKit

import RxSwift
import RxCocoa

import User
import Shared

public class LoginWebViewModel: EkscesoViewModel {
    
    public typealias DoneCallback = ((AuthorizationResult) -> Void)
    
    private enum Error: Swift.Error {
        case cantCreateURL
    }
    
    public var isActivityIndicatorAnimating: Signal<Bool> {
        return isActivityIndicatorAnimatingRelay.asSignal()
    }
    
    let isActivityIndicatorAnimatingRelay: PublishRelay<Bool> = .init()
    
    let onDoneCallback: DoneCallback
    
    init(userProvider: UserManaging, onDoneCallback: @escaping DoneCallback, disposeBag: DisposeBag) {
        self.onDoneCallback = onDoneCallback
        
        super.init(userProvider: userProvider, disposeBag: disposeBag)
    }
    
    public func getUrlRequest() throws -> URLRequest {
        guard let url = URL(string: ConfigUtils.AuthUrl + "/oauth2/authorize?client_id=" + ConfigUtils.ClientId + "&response_type=code") else {
            throw Error.cantCreateURL
        }
        
        return URLRequest(url: url)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        guard
            let url = navigationResponse.response.url,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let queryItems = components.queryItems
            else {
                decisionHandler(.cancel)
                
                return
        }
        
        for item in queryItems {
            if item.name == "code", let code = item.value {
                self.authorize(code: code)
                
                break
            }
        }
        
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let isActivityIndicatorVisible = webView.url?.absoluteString.contains(ConfigUtils.AuthUrl) == false
        
        self.isActivityIndicatorAnimatingRelay.accept(isActivityIndicatorVisible)
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Swift.Error) {
        isActivityIndicatorAnimatingRelay.accept(true)
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        isActivityIndicatorAnimatingRelay.accept(true)
    }
    
    func authorize(code: String) {
        userProvider
            .authorize(code: code)
            .subscribeCompleted { [weak self] (result) in
                self?.onDoneCallback(result)
            }
            .dispose(in: disposeBag)
    }
}
