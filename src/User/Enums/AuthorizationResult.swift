//
//  AuthorizationResult.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

public enum AuthorizationResult {
    
    case showLogin
    case showAccounts
    case showDashboard
}
