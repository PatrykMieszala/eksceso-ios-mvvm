//
//  UserProviderError.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 13.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation

enum UserProviderError: Error {
    case missingRefreshToken
}
