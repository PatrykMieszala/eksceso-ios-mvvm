//
//  UserProvider.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftDate
import Shared
import Api

public extension Notification.Name {
    static var colorModeChanged: Notification.Name {
        return Notification.Name("ColorModeChanged")
    }
}

class UserProvider: UserManaging {
    
    private struct Keys {
        static let user: String = "eksceso::user"
    }
    
    private var user: StoredUserModel = {
        return StoredUserModel.restore(withKey: Keys.user)
    }()
    {
        didSet {
            do {
                let data = try JSONEncoder().encode(user)
                UserDefaults.standard.set(data, forKey: Keys.user)
                log.info("Saving stored user")
                log.debug(user.toJSONString() ?? "Error when decoding to string")
            }
            catch let error {
                log.error(error)
            }
        }
    }
    
    var token: String? {
        return user.accessToken
    }
    
    var selectedAccounts: [AccountModel] {
        return user.selectedAccounts
    }
    
    var colorMode: ColorMode {
        get {
            return user.colorMode
        }
    }
    
    var interval: IntervalOption {
        get {
            return user.interval
        }
    }
    
    var daysOff: [Date] {
        get {
            return user.daysOff ?? []
        }
    }
    
    let shouldShowLogin: Signal<Void>
    private let shouldShowLoginRelay: PublishRelay<Void> = .init()
    
    static let shared: UserManaging = UserProvider()
    
    private lazy var api: UserApi = {
        return ApiFactory(tokenProvider: self, tokenRefresher: self).getUserApi()
    }()
    
    private init() {
        self.shouldShowLogin = self.shouldShowLoginRelay.asSignal()
    }
    
    func getToken() -> String? {
        return token
    }
    
    func refreshToken() -> Single<Void> {
        guard let token = self.user.refreshToken else {
            self.shouldShowLoginRelay.accept(())
            return .error(UserProviderError.missingRefreshToken)
        }
        
        return api
            .refresh(token: token)
            .doOnNext { [unowned self] (token) in
                self.update(token: token)
            }
            .doOnError { [unowned self] (error) in
                self.shouldShowLoginRelay.accept(())
            }
            .map { _ in return }
    }
    
    func authorize() -> Single<AuthorizationResult> {
        if token == nil {
            return .just(.showLogin)
        }
        
        if self.user.selectedAccounts.count == 0 {
            return .just(.showAccounts)
        }
        
        return .just(.showDashboard)
    }
    
    func authorize(code: String) -> Single<AuthorizationResult> {
        return api
            .authorize(code: code)
            .doOnNext { [unowned self] (token) in
                self.update(token: token)
            }
            .flatMap { [unowned self] _ in
                return self.authorize()
        }
    }
    
    func logout() {
        self.user = StoredUserModel()
        self.shouldShowLoginRelay.accept(())
    }
    
    func select(account: AccountModel) {
        if self.user.selectedAccounts.contains(account) == false {
            self.user.selectedAccounts.append(account)
        }
        
        self.save()
    }
    
    func unselect(account: AccountModel) {
        if let index = self.user.selectedAccounts.index(of: account) {
            self.user.selectedAccounts.remove(at: index)
        }
        
        self.save()
    }
    
    func set(interval: IntervalOption) {
        self.user.interval = interval
        
        self.save()
    }
    
    func set(colorMode: ColorMode) {
        self.user.colorMode = colorMode
        NotificationCenter.default.post(name: .colorModeChanged, object: nil, userInfo: nil)
        
        self.save()
    }
    
    func add(dayOff: Date) {
        var daysOff = self.daysOff
        
        if daysOff.contains(dayOff) == false {
            daysOff.append(dayOff)
            
            self.user.daysOff = daysOff
            
            self.save()
        }
    }
    
    func remove(dayOff: Date) {
        var daysOff = self.daysOff
        
        if let index = daysOff.index(of: dayOff) {
            daysOff.remove(at: index)
            
            self.user.daysOff = daysOff
            
            self.save()
        }
    }
    
    func isHoliday(forDate date: Date) -> Bool {
        return daysOff.contains {
            $0.isInside(date: date, granularity: .day)
        }
    }
    
    private func update(token: UserTokenResponse) {
        self.user.accessToken = token.accessToken
        self.user.refreshToken = token.refreshToken
        self.user.expires = token.expiresIn
        
        self.save()
    }
    
    private func save() {
        let tmp = user
        
        self.user = tmp
    }
}
