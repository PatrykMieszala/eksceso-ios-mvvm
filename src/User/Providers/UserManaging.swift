//
//  UserManaging.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa

import Api
import Shared

public protocol UserManaging: class, NetworkTokenProvider, NetworkTokenRefresher {
    var token: String? { get }
    var selectedAccounts: [AccountModel] { get }
    
    var colorMode: ColorMode { get }
    var interval: IntervalOption { get }
    var daysOff: [Date] { get }
    
    var shouldShowLogin: Signal<Void> { get }
    
    func authorize() -> Single<AuthorizationResult>
    func authorize(code: String) -> Single<AuthorizationResult>
    func refreshToken() -> Single<Void>
    
    func logout()
    
    ///Save selected company account
    func select(account: AccountModel)
    func unselect(account: AccountModel)
    func set(interval: IntervalOption)
    func set(colorMode: ColorMode)
    
    func add(dayOff: Date)
    func remove(dayOff: Date)
    func isHoliday(forDate date: Date) -> Bool
}
