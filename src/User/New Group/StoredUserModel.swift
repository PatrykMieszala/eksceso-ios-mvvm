//
//  StoredUserModel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import Foundation
import Shared
import Api

class StoredUserModel: Codable {
    
    var accessToken: String?
    var refreshToken: String?
    var expires: Int?
    var selectedAccounts: [AccountModel] = []
    
    var colorMode: ColorMode = .light
    var interval: IntervalOption = .thisMonth
    var daysOff: [Date]?
    
    static func restore(withKey key: String) -> StoredUserModel {
        guard
            let data = UserDefaults.standard.data(forKey: key),
            let user = try? JSONDecoder().decode(StoredUserModel.self, from: data)
            else {
                return StoredUserModel()
        }
        
        return user
    }
}
