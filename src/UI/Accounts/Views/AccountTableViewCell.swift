//
//  AccountTableViewCell.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import Domain
import Shared

class AccountTableViewCell: UITableViewCell {
    
    // - MARK: ibOutlet(s)
    @IBOutlet private weak var ibAccountName: UILabel!
    @IBOutlet private weak var ibDotView: BorderedView!
    @IBOutlet private weak var ibBottomLine: UIView!
    
    // - MARK: Public Variable(s)
    var viewModel: AccountCellViewModel! {
        didSet {
           initialize()
        }
    }
    
    // - MARK: View Method(s)
    private func initialize() {
        self.ibAccountName.text = viewModel.name
        self.ibBottomLine.isHidden = viewModel.isLast
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        ibDotView?.backgroundColor = isSelected ? Configuration.Colors.dotSelected.color : .clear
        ibDotView?.border = isSelected == false
        ibDotView?.layoutIfNeeded()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        ibAccountName.text = nil
        ibBottomLine.isHidden = true
    }

}
