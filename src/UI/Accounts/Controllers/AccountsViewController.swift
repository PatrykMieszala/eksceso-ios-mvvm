//
//  AccountsViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import Domain

class AccountsViewController: EkscesoViewController<AccountsViewModel>, UITableViewDelegate {
    
    // - MARK: ibOutlet(s)
    @IBOutlet private weak var ibTableView: UITableView!
    @IBOutlet private weak var ibDoneButton: UIButton!
    @IBOutlet private weak var ibUnderLine: UIView!
    @IBOutlet private weak var ibVerticalLine: UIView!
    
    // - MARK: Public Variable(s)
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // - MARK: Private Variable(s)
    
    // - MARK: View Method(s)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func bind(viewModel: AccountsViewModel) {
        super.bind(viewModel: viewModel)
        
        viewModel
            .feed
            .drive(ibTableView.rx.items) { tableView, row, viewModel in
                let indexPath = IndexPath(row: row, section: 0)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.accountTableViewCell, for: indexPath) !! "cell can't be nil"
                cell.viewModel = viewModel
                
                return cell
            }
            .dispose(in: disposeBag)
        
        viewModel
            .isDoneButtonHidden
            .emit(to: ibDoneButton.rx.animateHidden(duration: 0.2))
            .dispose(in: disposeBag)

        let isFeedEmpty = viewModel.isFeedEmpty

        isFeedEmpty
            .drive(ibUnderLine.rx.animateHidden())
            .dispose(in: disposeBag)

        isFeedEmpty
            .drive(ibVerticalLine.rx.animateHidden())
            .dispose(in: disposeBag)

        isFeedEmpty
            .drive(ibTableView.rx.animateHidden())
            .dispose(in: disposeBag)
        
        ibDoneButton
            .rx
            .tap
            .asSignal()
            .emit(onNext: { () in
                viewModel.didTapDoneButton()
            })
            .dispose(in: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRowAt(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        viewModel.didDeselectRowAt(indexPath: indexPath)
    }
}

// - MARK: private AccountsViewController extension
private extension AccountsViewController {
    
    func setup() {
        ibTableView.contentInset.bottom = 100
        ibTableView.allowsMultipleSelection = true
        ibTableView.isHidden = true
        ibTableView.alpha = 0
        
        ibTableView.rx.setDelegate(self).dispose(in: disposeBag)
        
        ibDoneButton.alpha = 0
        ibDoneButton.isHidden = true
        
        ibUnderLine.alpha = 0
        ibUnderLine.isHidden = true
        
        ibVerticalLine.alpha = 0
        ibVerticalLine.isHidden = true
    }
}
