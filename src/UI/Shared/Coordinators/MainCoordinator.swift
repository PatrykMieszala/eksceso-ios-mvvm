//
//  MainCoordinator.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 22.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import User
import Domain

public final class MainCoordinator: RxCoordinator {
    
    @IBOutlet private weak var navigationController: UINavigationController!
    
    private let window: UIWindow
    private let userFactory: UserFactory
    private let viewModelFactory: DomainFactory
    
    public init(window: UIWindow, userFactory: UserFactory, viewModelFactory: DomainFactory) {
        self.window = window
        self.userFactory = userFactory
        self.viewModelFactory = viewModelFactory
        
        super.init()
        
        userFactory
            .getUserProvider()
            .shouldShowLogin
            .emit(onNext: { [weak self] in
                self?.goToLogin(replaceRoot: true)
            })
            .dispose(in: disposeBag)
    }
    
    @discardableResult
    override public func start() -> UIViewController? {
        
        let nvc = EkscesoNavigationController()
        
        let splash = self.getSplash()
        
        nvc.setViewControllers([splash], animated: false)
        nvc.setNavigationBarHidden(true, animated: false)
        
        self.window.rootViewController = nvc
        self.window.makeKeyAndVisible()
        self.navigationController = nvc
        
        return splash
    }
}

private extension MainCoordinator {
    
    func getSplash() -> UIViewController {
        let splash: SplashViewController = R.storyboard.splash.splashViewController() !! "vc can't be nil"
        
        let viewModel = self.viewModelFactory.getSplashViewModel(
            onDoneCallback: { [weak self] (result) in
                switch result {
                case .showAccounts: self?.goToAccounts(animated: true)
                case .showDashboard: self?.goToDashboard(animated: true)
                case .showLogin: self?.goToLogin(replaceRoot: false)
                }
        },
            disposeBag: splash.disposeBag)
        
        splash.set(viewModel: viewModel)
        
        return splash
    }
    
    func goToLogin(replaceRoot: Bool) {
        let login: LoginViewController = R.storyboard.login.loginViewController() !! "vc can't be nil"
        login.set(viewModel: self.viewModelFactory.getViewModel(disposeBag: login.disposeBag))
        _ = login.view
        
        login
            .rx
            .onLoginTap
            .emit(onNext: { [weak self, weak login] in
                guard let self = self else { return }
                
                let webView = self.getLoginWebView(loginDoneCallback: { (result) in
                    switch result {
                    case .showAccounts:
                        login?.dismiss(animated: true, completion: nil)
                        self.goToAccounts(animated: true)
                    case .showDashboard:
                        self.goToDashboard(animated: true)
                        login?.dismiss(animated: true, completion: nil)
                    case .showLogin:
                        //This ideally shouldn't happen
                        login?.dismiss(animated: true, completion: nil)
                    }
                })
                
                login?.present(webView, animated: true, completion: nil)
            })
            .dispose(in: login.disposeBag)
        
        if replaceRoot {
            let nvc = EkscesoNavigationController()
            
            nvc.setViewControllers([login], animated: false)
            nvc.setNavigationBarHidden(true, animated: false)
            
            self.navigationController = nvc
            self.window.rootViewController = nvc
        }
        else {
            self.navigationController?.setViewControllers([login], animated: true)
        }
    }
    
    func getLoginWebView(loginDoneCallback: @escaping LoginWebViewModel.DoneCallback) -> UIViewController {
        let nvc = EkscesoNavigationController()
        
        let webView: LoginWebViewController = R.storyboard.login.loginWebViewController() !! "VC can't be nil"
        let viewModel: LoginWebViewModel = self.viewModelFactory.getLoginWebViewModel(onDoneCallback: loginDoneCallback, disposeBag: webView.disposeBag)
        
        webView.set(viewModel: viewModel)
        
        webView
            .rx
            .onCancel
            .emit(onNext: { [weak nvc] in
                nvc?.dismiss(animated: true, completion: nil)
            })
            .dispose(in: webView.disposeBag)
        
        nvc.setViewControllers([webView], animated: false)
        
        return nvc
    }
    
    func goToAccounts(animated: Bool) {
        let accounts = R.storyboard.accounts.accountsViewController() !! "VC can't be nil"
        
        let viewModel: AccountsViewModel = viewModelFactory
            .getAccountsViewModel(
                onDoneCallback: { [weak self] in
                    self?.goToDashboard(animated: true)
            },
                disposeBag: accounts.disposeBag)
        
        accounts.set(viewModel: viewModel)
        
        self.navigationController?.setViewControllers([accounts], animated: animated)
    }
    
    func goToDashboard(animated: Bool) {
        let dashboardCoordinator = DashboardCoordinator(parent: self, navigationController: self.navigationController, animated: animated, window: self.window, viewModelFactory: self.viewModelFactory)
        
        dashboardCoordinator.start()
    }
}
