//
//  EkscesoViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 07.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import Shared
import Domain

class EkscesoViewController<TViewModel: ViewModel>: UIViewController {
    
    typealias ViewModel = TViewModel
    
    private(set) var viewModel: ViewModel!
    
    func set(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    func bind(viewModel: ViewModel) { }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return Configuration.statusBarStyle(isLightMode: viewModel.isLightMode)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bind(viewModel: viewModel)
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillDisappear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }
}
