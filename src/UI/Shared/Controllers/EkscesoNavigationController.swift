//
//  EkscesoNavigationController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

class EkscesoNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return visibleViewController?.preferredStatusBarStyle ?? .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
        
        NotificationCenter
            .default
            .rx
            .notification(Notification.Name.colorModeChanged)
            .map { _ in return }
            .subscribeNext(setup)
            .dispose(in: disposeBag)
    }
    
    private func setup() {
        self.navigationBar.tintColor = Configuration.Colors.brand.color
        self.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: Configuration.Colors.brand.color,
            NSAttributedString.Key.font: Configuration.Fonts.navigationTitle.with(size: 18)
        ]
        
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    }
}
