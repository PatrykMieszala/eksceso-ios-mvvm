//
//  UIView+Rx.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: UIView {
    
    func animateHidden(duration: TimeInterval = 0.3) -> Binder<Bool> {
        return Binder(base) { view, hidden in
            if hidden == false && view.alpha == 0 {
                view.isHidden = false
            }
            
            UIView
                .animate(
                    withDuration: duration,
                    animations: {
                        view.alpha = hidden ? 0 : 1
                },
                    completion: { _ in
                        view.isHidden = hidden
                })
        }
    }
}
