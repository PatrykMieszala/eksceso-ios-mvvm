//
//  UIViewController+Rx.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

extension Reactive where Base: UIViewController {
    var dismissAnimated: Binder<Bool> {
        return Binder(base) { vc, animated in
            vc.dismiss(animated: animated, completion: nil)
        }
    }
    
    func dismiss(animated: Bool) -> Driver<Void> {
        return Observable
            .create { [weak vc = self.base] (observer: AnyObserver<Void>) -> Disposable in
                vc?
                    .dismiss(animated: animated) {
                        observer.onNext(())
                }
                
                return Disposables.create()
            }
            .asDriver(onErrorJustReturn: ())
    }
    
    var viewDidDismissAsPopup: ControlEvent<Void> {
        let source = methodInvoked(#selector(Base.viewDidDismiss))
            .map { _ in return }
        
        return ControlEvent(events: source)
    }
}
