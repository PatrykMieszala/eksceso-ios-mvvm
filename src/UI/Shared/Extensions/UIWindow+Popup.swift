//
//  UIWindow+Popup.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 05.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIWindow {
    
    func presentPopup(viewController: UIViewController, topMargin: CGFloat = 150) {
        
        guard let view = viewController.view, let rootViewController = self.rootViewController else { return }
        
        view.frame.size.width -= 10
        view.frame.size.height -= topMargin
        view.center = self.center
        view.frame.origin.y = self.frame.height
        
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        
        self.addSubview(view)
        
        rootViewController.view.isUserInteractionEnabled = false
        
        self.addBlur(below: view)
        self.animateBlur(style: .regular, withDuration: 0.3, delay: 0.1, show: true, completion: nil)
        
        let tap = UITapGestureRecognizer()
        
        tap
            .rx
            .event
            .asObservable()
            .subscribeNext { [weak self, weak viewController] _ in
                guard let `self` = self, let viewController = viewController, viewController.dismissOnTap == true else { return }
                self.dismissPopup(viewController: viewController)
            }
            .dispose(in: disposeBag)
        
        self.blur?.addGestureRecognizer(tap)
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 3, options: .allowUserInteraction, animations: {
            view.frame.origin.y = topMargin
            
            rootViewController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            
            UIApplication.shared.statusBarStyle = .lightContent
            
            viewController.viewWillAppear(true)
        }, completion: { flag in
            viewController.viewDidAppear(true)
        })
    }
    
    func dismissPopup(viewController: UIViewController) {
        
        guard let view = viewController.view, let rootViewController = self.rootViewController else { return }
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 3, options: .allowUserInteraction, animations: {
            view.frame.origin.y = self.frame.height
            
            rootViewController.view.transform = CGAffineTransform.identity
            
            UIApplication.shared.statusBarStyle = .default
            
            viewController.viewWillDisappear(true)
        }, completion: { flag in
            view.removeFromSuperview()
            viewController.viewDidDisappear(true)
            viewController.viewDidDismiss()
            rootViewController.view.isUserInteractionEnabled = true
        })
        
        self.animateBlur(show: false) { [weak self] (_) in
            self?.removeBlur()
        }
    }
}

extension Reactive where Base: UIWindow {
    var dismissPopup: Binder<UIViewController> {
        return Binder(base) { window, vc in
            window.dismissPopup(viewController: vc)
        }
    }
}
