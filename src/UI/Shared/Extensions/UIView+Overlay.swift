//
//  UIView+Overlay.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 05.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit

fileprivate struct OverlayConstants {
    static let overlayTag = 8888
    static let blurTag = 9999
}

fileprivate typealias C = OverlayConstants

extension UIView {
    
    var overlay: UIView? {
        for view in subviews where view.tag == C.overlayTag {
            return view
        }
        
        return nil
    }
    
    var blur: UIView? {
        for view in subviews where view.tag == C.blurTag {
            return view
        }
        
        return nil
    }
    
    //- MARK: overlay
    func addOverlay(color: UIColor = UIColor.black.withAlphaComponent(0.4), below: UIView? = nil, above: UIView? = nil) {
        
        for view in subviews where view.tag == C.overlayTag {
            return
        }
        
        let overlay = UIView(frame: self.bounds)
        overlay.tag = C.overlayTag
        overlay.backgroundColor = color
        overlay.alpha = 0
        
        if let below = below {
            self.insertSubview(overlay, belowSubview: below)
        }
        else if let above = above {
            self.insertSubview(overlay, aboveSubview: above)
        } else {
            self.addSubview(overlay)
        }
    }
    
    func animateOverlay(withDuration duration: Double = 0.3, show: Bool, completion: ((Bool) -> Void)?) {
        for case let view in subviews where view.tag == C.overlayTag {
            UIView.animate(withDuration: duration, animations: {
                view.alpha = show ? 1 : 0
            }, completion: completion)
        }
    }
    
    func removeOverlay() {
        for case let view in subviews where view.tag == C.overlayTag {
            view.removeFromSuperview()
        }
    }
    
    //- MARK: blur
    func addBlur(below: UIView? = nil, above: UIView? = nil) {
        
        for view in subviews where view.tag == C.blurTag {
            return
        }
        
        let blur = UIVisualEffectView(frame: self.bounds)
        blur.tag = C.blurTag
        
        if let below = below {
            self.insertSubview(blur, belowSubview: below)
        }
        else if let above = above {
            self.insertSubview(blur, aboveSubview: above)
        } else {
            self.addSubview(blur)
        }
    }
    
    func animateBlur(style: UIBlurEffect.Style = .dark, withDuration duration: Double = 0.3, delay: Double = 0, show: Bool, completion: ((Bool) -> Void)?) {
        for case let view in subviews where view.tag == C.blurTag {
            guard let blur = view as? UIVisualEffectView else { return }
            UIView.animate(withDuration: duration, delay: delay, animations: {
                let effect = UIBlurEffect(style: style)
                blur.effect = show ? effect : nil
            }, completion: completion)
        }
    }
    
    func removeBlur() {
        for case let view in subviews where view.tag == C.blurTag {
            view.removeFromSuperview()
        }
    }
}
