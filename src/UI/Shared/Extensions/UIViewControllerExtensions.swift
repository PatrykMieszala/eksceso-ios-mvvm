//
//  UIViewControllerExtensions.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 05.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift

private struct AssociatedKeys {
    static var DismissOnTapKey = "DismissOnTapKey"
}

public extension UIViewController {
    
    func setupEndEditingOnTap() {
        let tap = UITapGestureRecognizer()
        self.view.addGestureRecognizer(tap)
        
        tap
            .rx
            .event
            .subscribeNext { [weak self] (tap) in
                self?.view.endEditing(true)
            }
            .dispose(in: disposeBag)
    }
    
    ///Function calls `reactive.viewDismissed`.
    ///
    ///NOTE: Remember to be sure that viewController is UIViewController or UINavigationController.
    @objc func viewDidDismiss() { }
    
    public var dismissOnTap: Bool {
        get {
            if let dismissOnTap = objc_getAssociatedObject(self, &AssociatedKeys.DismissOnTapKey) as? Bool {
                return dismissOnTap
            } else {
                let dismissOnTap = false
                objc_setAssociatedObject(self, &AssociatedKeys.DismissOnTapKey, dismissOnTap, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return dismissOnTap
            }
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.DismissOnTapKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
