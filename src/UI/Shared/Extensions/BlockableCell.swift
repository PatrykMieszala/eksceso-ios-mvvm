//
//  BlockableCell.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 21/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit

@objc protocol BlockableCell: class {
    @objc func blockAnyChanges()
}

extension UITableViewCell: BlockableCell {
    func blockAnyChanges() { }
}
