//
//  EkscesoDotView.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

class EkscesoDotView: BorderedView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setup()
    }
    
    override func onColorModeChaged(notification: Notification) {
        super.onColorModeChaged(notification: notification)
        
        self.setup()
    }
    
    private func setup() {
        self.borderColor = Configuration.Colors.dotBorder.color
    }
}
