//
//  EkscesoView.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 21/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxCocoa

class EkscesoView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter
            .default
            .rx
            .notification(Notification.Name.colorModeChanged)
            .subscribeNext(onColorModeChaged)
            .dispose(in: disposeBag)
    }
    
    func onColorModeChaged(notification: Notification) { }
}
