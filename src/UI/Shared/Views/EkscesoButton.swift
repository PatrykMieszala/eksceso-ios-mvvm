//
//  EkscesoButton.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

@IBDesignable
class EkscesoButton: UIButton {
    
    @IBInspectable private var fontType: String = "primaryButton"
    @IBInspectable private var textColorType: String = "primaryButtonTitle"
    @IBInspectable private var backgroundColorType: String?
    
    @IBInspectable private var radius: CGFloat = 0
    @IBInspectable private var borderColor: UIColor?
    @IBInspectable private var borderWidth: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter
            .default
            .rx
            .notification(Notification.Name.colorModeChanged)
            .map { _ in return }
            .subscribeNext(setup)
            .dispose(in: disposeBag)
        
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        self.setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = borderWidth
        
        self.backgroundColor = Configuration.Colors(rawValue: backgroundColorType ?? "")?.color
        
        self.setTitleColor(Configuration.Colors(raw: textColorType).color, for: .normal)
        
        let size = self.titleLabel?.font.pointSize ?? 18
        self.titleLabel?.font = Configuration.Fonts(raw: fontType).with(size: size)
    }
}
