//
//  BorderedView.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 31.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

@IBDesignable
class BorderedView: EkscesoView {
    
    @IBInspectable var border: Bool = false
    @IBInspectable var borderColor: UIColor = Configuration.Colors.border.color
    @IBInspectable var borderWidth: CGFloat = 0.5
    
    @IBInspectable var radius: CGFloat = 6
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        self.setup()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        self.setup()
    }
    
    override func onColorModeChaged(notification: Notification) {
        super.onColorModeChaged(notification: notification)
        
        self.setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = radius
        
        if border {
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = borderWidth
        }
        else {
            self.layer.borderWidth = 0
        }
    }
    
}
