//
//  EkscesoLoadingLabel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 20/10/2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

class EkscesoLoadingLabel: EkscesoLabel {
    
    private weak var gradient: CAGradientLayer?
    private weak var gradientChangeAnimation: CABasicAnimation?
    private weak var gradientLocationAnimation: CABasicAnimation?
    
    private let gradientAnimationKey: String = "colorChange"
    private let gradientLocationAnimationKey: String = "locationChange"
    
    func startLoading() {
        self.sizeToFit()
        self.layoutSubviews()
        
        if self.gradient == nil {
            let gradient = CAGradientLayer()
            
            gradient.frame = self.bounds
            gradient.cornerRadius = gradient.bounds.height / 2
            gradient.masksToBounds = true
            gradient.colors = [
                Configuration.Colors.brand.color.cgColor,
                UIColor.white.withAlphaComponent(0).cgColor
            ]
            gradient.locations = [0, 0.2]
            gradient.startPoint = CGPoint(x:0, y:0)
            gradient.endPoint = CGPoint(x:1, y:0)
            
            self.layer.addSublayer(gradient)
            
            self.gradient = gradient
        }
        
        if self.gradientChangeAnimation == nil {
            let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
            gradientChangeAnimation.duration = 1
            gradientChangeAnimation.toValue = [
                UIColor.white.withAlphaComponent(0).cgColor,
                Configuration.Colors.brand.color.cgColor
            ]
            gradientChangeAnimation.fillMode = .forwards
            gradientChangeAnimation.isRemovedOnCompletion = false
            gradientChangeAnimation.autoreverses = true
            gradientChangeAnimation.repeatCount = .infinity
            
            self.gradient?.add(gradientChangeAnimation, forKey: gradientAnimationKey)
            
            self.gradientChangeAnimation = gradientChangeAnimation
        }
        
        if self.gradientLocationAnimation == nil {
            let gradientLocationAnimation = CABasicAnimation(keyPath: "locations")
            gradientLocationAnimation.duration = 1
            gradientLocationAnimation.toValue = [0.8, 1]
            gradientLocationAnimation.fillMode = .forwards
            gradientLocationAnimation.isRemovedOnCompletion = false
            gradientLocationAnimation.autoreverses = true
            gradientLocationAnimation.repeatCount = .infinity
            
            self.gradient?.add(gradientLocationAnimation, forKey: gradientLocationAnimationKey)
            
            self.gradientLocationAnimation = gradientLocationAnimation
        }
    }
    
    func stopLoading() {
        self.gradient?.removeAnimation(forKey: gradientAnimationKey)
        self.gradient?.removeAnimation(forKey: gradientLocationAnimationKey)
        self.gradient?.removeFromSuperlayer()
        
        self.gradientChangeAnimation = nil
        self.gradientLocationAnimation = nil
        self.gradient = nil
    }
}
