//
//  EkscesoGradientView.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Shared

class EkscesoGradientView: EkscesoView {
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        self.setup()
    }
    
    override func onColorModeChaged(notification: Notification) {
        super.onColorModeChaged(notification: notification)
        
        self.setup()
    }

    private func setup() {
        let first = Configuration.Colors.primaryBackground.color
        let second = Configuration.Colors.primaryBackground.color
        //Yeah, not really gradient, but ¯\_(ツ)_/¯
        
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [first.cgColor, second.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
    }

}
