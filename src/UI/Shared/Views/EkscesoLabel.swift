//
//  EkscesoLabel.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 27.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxCocoa
import Shared

@IBDesignable
class EkscesoLabel: UILabel {
    
    @IBInspectable private var fontType: String = "primaryText"
    @IBInspectable private var textColorType: String = "primaryText"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter
            .default
            .rx
            .notification(Notification.Name.colorModeChanged)
            .map { _ in return }
            .subscribeNext(setup)
            .dispose(in: disposeBag)
        
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        self.setup()
    }
    
    private func setup() {
        self.textColor = Configuration.Colors(raw: textColorType).color
        
        let size = self.font.pointSize
        self.font = Configuration.Fonts(raw: fontType).with(size: size)
    }
    
    func setFont(size: CGFloat) {
        self.font = Configuration.Fonts(raw: fontType).with(size: size)
    }
}
