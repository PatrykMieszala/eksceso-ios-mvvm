//
//  DashboardCoordinator.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import Domain
import Api

final class DashboardCoordinator: RxCoordinator {
    
    @IBOutlet fileprivate weak var navigationController: UINavigationController!
    
    private let animated: Bool
    private let window: UIWindow
    private let viewModelFactory: DomainFactory
    
    init(parent: RxCoordinator,
         navigationController: UINavigationController?,
         animated: Bool,
         window: UIWindow,
         viewModelFactory: DomainFactory
        ) {
        self.animated = animated
        self.window = window
        self.viewModelFactory = viewModelFactory
        
        super.init(parent: parent)
        self.navigationController = navigationController
    }
    
    @discardableResult
    override func start() -> UIViewController? {
        let dashboard = R.storyboard.dashboard.dashboardViewController() !! "VC can't be nil"
        
        let viewModel: DashboardViewModel = viewModelFactory
            .getDashboardViewModel(
                goToSettingsCallback: { [weak self] accounts in
                    self?.goToSettings(accounts: accounts)
            },
                disposeBag: dashboard.disposeBag)
        
        dashboard.set(viewModel: viewModel)
        
        self.navigationController?.setViewControllers([dashboard], animated: animated)
        
        return dashboard
    }
}

private extension DashboardCoordinator {
    
    func goToSettings(accounts: AccountsResponseModel) {
        let settings = R.storyboard.dashboardSettings.dashboardSettingsViewController() !! "VC can't be nil"
        
        let viewModel: DashboardSettingsViewModel = viewModelFactory
            .getDashboardSettingsViewModel(accountModel: accounts, disposeBag: settings.disposeBag)
        
        settings.set(viewModel: viewModel)
        
        self.navigationController?.pushViewController(settings, animated: true)
    }
//
//    var navigateToAccounts: Route<Void> {
//        return route(parent: self) { (this, _) in
//            let accounts = Accounts.Coordinator(parent: this, parentController: this.navigationController, animated: true)
//            let vc = accounts.start()
//
//            accounts
//                .onDone
//                .driveNext { [weak vc] in
//                    vc?.dismiss(animated: true, completion: nil)
//                }
//                .dispose(in: accounts.disposeBag)
//
//            return accounts
//        }
//    }
}
