//
//  DashboardViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import RxDataSources
import Charts

import Domain
import Shared

private struct DashboardViewControllerConstants {
    static let charMinHeightProportion: CGFloat = 0.3
    static let charMaxHeightProportionConstant: CGFloat = 0
    
    static let intervalMinFontSize: CGFloat = 14
    static let intervalMaxFontSizeRatio: CGFloat = 2.5
    static let intervalBottomInset: CGFloat = 20
}

private typealias C = DashboardViewControllerConstants

class DashboardViewController: EkscesoViewController<DashboardViewModel>, ChartViewDelegate, UIScrollViewDelegate, UITableViewDelegate {
    
    // - MARK: ibOutlet(s)
    @IBOutlet fileprivate weak var ibChartContainerView: UIView!
    @IBOutlet fileprivate weak var ibChartToSuperviewProportion: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var ibPieChartView: PieChartView!
    @IBOutlet fileprivate weak var ibPieChartTop: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var ibTableView: UITableView!
    
    @IBOutlet fileprivate weak var ibIntervalLabel: EkscesoLabel!
    @IBOutlet fileprivate weak var ibIntervalLabelY: NSLayoutConstraint!
    
    // - MARK: Public Variable(s)
    
    // - MARK: Private Variable(s)
    private var timer: Timer?
    
    fileprivate let chartTextAlpha: PublishSubject<CGFloat> = .init()
    fileprivate let chartHeightDelta: PublishSubject<CGFloat> = .init()
    fileprivate let intervalY: PublishSubject<CGFloat> = .init()
    
    private lazy var tableDataSource: RxTableViewSectionedAnimatedDataSource<AnimatableSectionModel<String, DashboardTaskCellViewModel>> = {
        let tableDataSource = RxTableViewSectionedAnimatedDataSource<AnimatableSectionModel<String, DashboardTaskCellViewModel>>(configureCell: { dataSource, tableView, indexPath, viewModel in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.dashboardTaskTableViewCell, for: indexPath) !! "cell can't be nil"
            cell.viewModel = viewModel
            
            return cell
        })
        
        tableDataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .fade, reloadAnimation: .fade, deleteAnimation: .fade)
        
        return tableDataSource
    }()
    
    // - MARK: View Method(s)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func bind(viewModel: DashboardViewModel) {
        viewModel.refreshCommand.state += (self, DashboardViewController.onLoadTable)
        viewModel.refreshCommand.state += (self, DashboardViewController.onLoadChart)
        viewModel.shareCSVCommand.success += (self, DashboardViewController.onShareCSV)

        viewModel
            .chartEntries
            .driveNext { [weak self] (entries) in
                self?.setChart(entries: entries)
            }
            .dispose(in: disposeBag)

        viewModel
            .intervalText
            .drive(ibIntervalLabel.rx.text)
            .dispose(in: disposeBag)
        
        viewModel
            .tableFeed
            .drive(ibTableView.rx.items(dataSource: tableDataSource))
            .dispose(in: disposeBag)
        
        chartTextAlpha
            .subscribeNext { [weak self] (alpha) in
                self?
                    .ibPieChartView
                    .data?
                    .dataSets
                    .forEach { (set) in
                        guard let set = set as? PieChartDataSet else { return }
                        set.entryLabelColor = UIColor.white.withAlphaComponent(alpha)
                        set.valueTextColor = UIColor.lightText.withAlphaComponent(alpha)
                }
            }
            .dispose(in: disposeBag)
        
        let intervalYFromChartHeight = chartHeightDelta
            .map { [weak self] (delta) -> CGFloat in
                let chartHeight = self?.ibPieChartView.frame.height ?? 0
                let newY = ((chartHeight / 2) - C.intervalBottomInset) * delta
                
                return newY
            }
        
        let intervalYMerged = Observable
            .merge(intervalYFromChartHeight, intervalY)
            
        intervalYMerged
            .subscribeNext { [weak self] (newY) in
                let viewHeight = self?.view.frame.height ?? 0
                let chartMaxHeight = viewHeight / 2
                let ratio = 1 + ((chartMaxHeight - newY) / chartMaxHeight)
                
                let size: CGFloat = C.intervalMinFontSize * ratio
                
                self?.ibIntervalLabel.setFont(size: size)
                
                self?.ibIntervalLabelY.constant = newY
            }
            .dispose(in: disposeBag)
        
        Driver
            .combineLatest(
                viewModel.hoursAmount,
                viewModel.maxHours,
                viewModel.hoursColor,
                chartTextAlpha.asDriver(onErrorJustReturn: 1)
            ) { [weak self] (hoursAmount, maxHours, hoursColor, alpha) -> NSAttributedString in
                //TODO: This should be in viewModel
                
                let viewHeight = self?.view.frame.height ?? 0
                let chartMaxHeight = viewHeight / 2
                let chartHeight = self?.ibChartContainerView.frame.height ?? 0
                let ratio = chartHeight / chartMaxHeight

                let text = hoursAmount + maxHours

                let paragraph = NSMutableParagraphStyle()
                paragraph.alignment = .center

                let hourAtts: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.font: Configuration.Fonts.chartHours.with(size: 28 * ratio),
                    NSAttributedString.Key.foregroundColor: hoursColor.withAlphaComponent(alpha),
                    NSAttributedString.Key.paragraphStyle: paragraph
                ]
                let maxAtts: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.font: Configuration.Fonts.chartMaxHours.with(size: 11 * ratio),
                    NSAttributedString.Key.foregroundColor: Configuration.Colors.maxHoursText.color.withAlphaComponent(alpha),
                    NSAttributedString.Key.paragraphStyle: paragraph
                ]

                let attString = NSMutableAttributedString(string: text)

                text.enumerateSubstrings(in: text.startIndex..<text.endIndex, options: .byLines) { (string, range, _, _) in
                    if string == hoursAmount {
                        attString.addAttributes(hourAtts, range: NSRange(range, in: text))
                    }
                    else {
                        attString.addAttributes(maxAtts, range: NSRange(range, in: text))
                    }
                }

                return attString
            }
            .driveNext { [weak self] (attString) in
                self?.ibPieChartView.centerAttributedText = attString
            }
            .dispose(in: disposeBag)
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let entry = entry as? DashboardChartEntry
        self.viewModel.filterByChartEntryCommand.execute(entry)
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        self.viewModel.filterByChartEntryCommand.execute(nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentOffset = ibTableView.contentOffset
        let constant = ibChartToSuperviewProportion.constant
        let newConstant = constant - contentOffset.y
        
        let minHeight = -(view.frame.height * C.charMinHeightProportion)
        
        guard newConstant > minHeight else {
            ibChartToSuperviewProportion.constant = minHeight
            chartHeightDelta.onNext(0)
            chartTextAlpha.onNext(0)
            return
        }
        
        guard newConstant < C.charMaxHeightProportionConstant else {
            ibChartToSuperviewProportion.constant = C.charMaxHeightProportionConstant
            chartHeightDelta.onNext(1)
            chartTextAlpha.onNext(1)
            return
        }
        
        ibChartToSuperviewProportion.constant -= contentOffset.y
        ibTableView.contentOffset.y = 0
        
        let diff = abs(newConstant/minHeight)
        let delta = max(1 - diff, 0)
        let alpha = max(0.5 - diff, 0)
        
        chartHeightDelta.onNext(delta)
        chartTextAlpha.onNext(alpha)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = R.nib.dashboardSectionHeader.firstView(owner: nil)
        
        view?.titleLabel.text = viewModel.sectionTitle(forSection: section)
        view?.hoursLabel.text = viewModel.sectionHoursString(forSection: section)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

// - MARK: private DashboardViewController extension
private extension DashboardViewController {
    
    func onLoadTable(state: RxCommandState) {
        switch state {
        case .executing:
            ibTableView.refreshControl?.beginRefreshing()
        case .finish:
            ibTableView.refreshControl?.endRefreshing()
        }
    }
    
    func onLoadChart(state: RxCommandState) {
        self.timer?.invalidate()
        self.timer = nil
        
        switch state {
        case .executing:
            self.ibPieChartView.spin(duration: 1, fromAngle: 0, toAngle: 360)
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
                self?.ibPieChartView.spin(duration: 1, fromAngle: 0, toAngle: 360)
            }
        case .finish:
            break
        }
    }
    
    func onShareCSV(url: URL) {
        let activity = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        
        present(activity, animated: true, completion: nil)
    }
    
    func setup() {
        self.ibTableView.register(R.nib.dashboardTaskTableViewCell)
        
        ibTableView
            .rx
            .itemSelected
            .subscribeNext { tableView, indexPath in
                tableView.deselectRow(at: indexPath, animated: true)
            }
            .dispose(in: disposeBag)
        
        ibTableView
            .rx
            .setDelegate(self)
            .dispose(in: disposeBag)
        
        Driver
            .merge(
                [
                    ibTableView
                        .rx
                        .didEndDecelerating
                        .asDriver(),
                    ibTableView
                        .rx
                        .didEndDragging
                        .asDriver()
                        .filter { $0 == false }
                        .map { _ in return },
                    rx
                        .viewWillAppear
                        .asDriver()
                        .map { _ in return }
                ]
            )
            .debounce(0.2)
            .driveNext { [weak self] in
                self?.decideAboutHeaderHeight()
            }
            .dispose(in: disposeBag)
        
        let tap = UITapGestureRecognizer()
        
        tap
            .rx
            .event
            .subscribeNext { [weak self] (tap) in
                self?.ibPieChartView.highlightValue(nil, callDelegate: true)
            }
            .dispose(in: disposeBag)
        
        ibChartContainerView.addGestureRecognizer(tap)
        
        self.setupChart()
        self.setupRefresh()
        self.setupSwitches()
        self.setupNavigationButtons()
    }
    
    func setupChart() {
        let chartView: PieChartView = ibPieChartView
        
        chartView.delegate = self
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.40
        chartView.transparentCircleRadiusPercent = 0
        chartView.holeColor = .clear
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 5, right: 0, bottom: 5)
        
        chartView.drawCenterTextEnabled = true
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = true
        chartView.maxHighlightDistance = 0
        
        let legend = ibPieChartView.legend
        legend.horizontalAlignment = .left
        legend.verticalAlignment = .top
        legend.orientation = .vertical
        legend.xEntrySpace = 7
        legend.yEntrySpace = 0
        legend.yOffset = 0
        legend.drawInside = true
        legend.wordWrapEnabled = true
        legend.form = .circle
        legend.enabled = false
    }
    
    func setupRefresh() {
        let refresh = UIRefreshControl()
        refresh.tintColor = Configuration.Colors.brand.color
        ibTableView.refreshControl = refresh
        
        //HACK: iOS has bug, that refreshControl color is not properly set when first calling refreshControl.beginAnimating()
        ibTableView.contentOffset = CGPoint(x:0, y: -refresh.frame.size.height)
        
        refresh
            .rx
            .controlEvent(.valueChanged)
            .filter { [unowned refresh] in
                return refresh.isRefreshing
            }
            .bind(to: viewModel.refreshCommand)
            .dispose(in: disposeBag)
    }
    
    func setupSwitches() {
        let segmentedControl = UISegmentedControl()
        segmentedControl.insertSegment(withTitle: DashboardSortType.byDay.text, at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: DashboardSortType.byProject.text, at: 1, animated: false)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.apportionsSegmentWidthsByContent = false
        segmentedControl.setWidth(80, forSegmentAt: 0)
        segmentedControl.setWidth(80, forSegmentAt: 1)
        
        segmentedControl
            .rx
            .selectedSegmentIndex
            .asObservable()
            .map { index -> DashboardSortType in
                return DashboardSortType(rawValue: index) !! "This can't happen"
            }
            .bind(to: viewModel.changeSortCommand)
            .dispose(in: disposeBag)
        
        self.navigationItem.titleView = segmentedControl
    }
    
    func setupNavigationButtons() {
        let settingsButton = UIBarButtonItem(image: R.image.settingsIcon(), style: .plain, target: nil, action: nil)
        
        settingsButton
            .rx
            .tap
            .subscribeNext { [weak self] in
                self?.viewModel.didTapSettings()
            }
            .dispose(in: disposeBag)
        
        self.navigationItem.rightBarButtonItem = settingsButton
        
        let shareButton = UIBarButtonItem(image: R.image.shareIcon(), style: .plain, target: nil, action: nil)
        
        shareButton
            .rx
            .tap
            .bind(to: viewModel.shareCSVCommand)
            .dispose(in: disposeBag)
        
        self.navigationItem.leftBarButtonItem = shareButton
    }
    
    func setChart(entries: [DashboardChartEntry]) {
        let set = PieChartDataSet(values: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 2
        
        set.colors = entries.compactMap { $0.color }
        set.entryLabelColor = Configuration.Colors.primaryText.color
        set.valueTextColor = Configuration.Colors.secondaryText.color
        
        set.valueFont = FontFamily.Avenir.heavy.font(withSize: 13)
        set.entryLabelFont =  FontFamily.Avenir.light.font(withSize: 11)
        
        let data = PieChartData(dataSet: set)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.maximumFractionDigits = 1
        numberFormatter.multiplier = 1
        numberFormatter.percentSymbol = " %"
        
        ibPieChartView.data = data
        ibPieChartView.highlightValues(nil)
        
        if entries.contains(where: { $0.value == Double.pi }) {
            set.drawValuesEnabled = false
            set.colors = [Configuration.Colors.brand.color]
            set.sliceSpace = 5
            ibPieChartView.notifyDataSetChanged()
        } else {
            data.setValueFormatter(DefaultValueFormatter(formatter: numberFormatter))
            ibPieChartView.animate(xAxisDuration: 1, yAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
        }
    }
    
    func decideAboutHeaderHeight() {
        let headerHeight = ibChartContainerView.frame.height
        let superHeight = view.frame.height
        
        let ratio = headerHeight/superHeight
        let show: Bool = ratio > 0.3
        
        let headerRatioConstant: CGFloat = show ? 0 : -(superHeight * C.charMinHeightProportion)
        ibChartToSuperviewProportion.constant = headerRatioConstant
        
        let showHeaderIntervalY: CGFloat = (superHeight / 4) - C.intervalBottomInset
        let intervalLabelY: CGFloat = show ? showHeaderIntervalY : 0
        self.intervalY.onNext(intervalLabelY)
        
        let alpha: CGFloat = show ? 1 : 0
        
        UIView
            .animate(withDuration: 0.25,
                     delay: 0,
                     options: .curveEaseIn,
                     animations: { [weak self] in
                        self?.view.layoutIfNeeded()
                        self?.chartTextAlpha.onNext(alpha)
                },
                     completion: nil)
    }
}
