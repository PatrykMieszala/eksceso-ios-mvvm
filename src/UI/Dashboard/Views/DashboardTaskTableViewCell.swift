//
//  DashboardTaskTableViewCell.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 03.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import Domain

class DashboardTaskTableViewCell: UITableViewCell {
    
    // - MARK: ibOutlet(s)
    @IBOutlet private weak var ibClientLabel: EkscesoLoadingLabel!
    @IBOutlet private weak var ibProjectLabel: EkscesoLoadingLabel!
    @IBOutlet private weak var ibTaskTypeLabel: EkscesoLoadingLabel!
    @IBOutlet private weak var ibTaskNameLabel: EkscesoLoadingLabel!
    @IBOutlet private weak var ibTimeLabel: EkscesoLoadingLabel!
    @IBOutlet private weak var ibLineHeight: NSLayoutConstraint!
    @IBOutlet private weak var ibDateLabel: EkscesoLoadingLabel!
    
    // - MARK: Public Variable(s)
    
    var viewModel: DashboardTaskCellViewModel! {
        didSet {
           initialize()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ibLineHeight.constant = (1 / UIScreen.main.scale)
    }
    
    // - MARK: View Method(s)
    private func initialize() {
        ibProjectLabel.textColor = viewModel.color
        ibProjectLabel.text = viewModel.projectName
        ibClientLabel.text = viewModel.clientName
        ibTaskTypeLabel.text = viewModel.taskType
        ibTaskNameLabel.text = viewModel.taskName
        ibTimeLabel.text = viewModel.time
        ibDateLabel.text = viewModel.spentDateString
        ibDateLabel.isHidden = viewModel.isDateHidden
        
        if viewModel.isLoading {
            ibProjectLabel.startLoading()
            ibClientLabel.startLoading()
            ibTaskTypeLabel.startLoading()
            ibTaskNameLabel.startLoading()
            ibTimeLabel.startLoading()
            ibDateLabel.startLoading()
        } else {
            ibProjectLabel.stopLoading()
            ibClientLabel.stopLoading()
            ibTaskTypeLabel.stopLoading()
            ibTaskNameLabel.stopLoading()
            ibTimeLabel.stopLoading()
            ibDateLabel.stopLoading()
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
