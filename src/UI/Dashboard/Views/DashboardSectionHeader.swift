//
//  DashboardSectionHeader.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 27.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Domain
import Shared

class DashboardSectionHeader: UIView {

    @IBOutlet weak var titleLabel: EkscesoLabel!
    @IBOutlet weak var hoursLabel: EkscesoLabel!
    @IBOutlet weak var line: EkscesoLineView!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lineHeight.constant = (1 / UIScreen.main.scale)
        self.backgroundColor = Configuration.Colors.primaryBackground.color
    }
}
