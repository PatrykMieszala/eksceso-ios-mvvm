//
//  LoginViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 22.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import Domain

class LoginViewController: EkscesoViewController<EkscesoViewModel> {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // - MARK: ibOutlet(s)
    @IBOutlet fileprivate weak var ibLoginButton: EkscesoButton!
}

extension Reactive where Base: LoginViewController {
    var onLoginTap: Signal<Void> {
        return base.ibLoginButton.rx.tap.asSignal()
    }
}
