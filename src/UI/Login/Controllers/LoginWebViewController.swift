//
//  LoginWebViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 28.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import WebKit
import Domain

class LoginWebViewController: EkscesoViewController<LoginWebViewModel>, WKNavigationDelegate, WKUIDelegate {
    
    // - MARK: ibOutlet(s)
    @IBOutlet fileprivate weak var ibWebView: WKWebView!
    @IBOutlet fileprivate weak var ibActivityIndicatorView: UIActivityIndicatorView!
    
    // - MARK: Public Variable(s)
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // - MARK: View Method(s)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func bind(viewModel: LoginWebViewModel) {
        super.bind(viewModel: viewModel)
        
        ibWebView.load(try! viewModel.getUrlRequest())
        
        viewModel
            .isActivityIndicatorAnimating
            .emit(to: ibActivityIndicatorView.rx.isAnimating)
            .dispose(in: disposeBag)
        
        viewModel
            .isActivityIndicatorAnimating
            .emit(to: ibWebView.rx.animateHidden())
            .dispose(in: disposeBag)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        viewModel.webView(webView, decidePolicyFor: navigationResponse, decisionHandler: decisionHandler)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        viewModel.webView(webView, didFinish: navigation)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        viewModel.webView(webView, didFail: navigation, withError: error)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        viewModel.webView(webView, didStartProvisionalNavigation: navigation)
    }
}

// - MARK: private LoginWebViewController extension
private extension LoginWebViewController {
    
    func setup() {
        ibWebView.allowsBackForwardNavigationGestures = true
        ibWebView.uiDelegate = self
        ibWebView.navigationDelegate = self
        ibWebView.isHidden = true
        
        ibActivityIndicatorView.hidesWhenStopped = true
    }
}

extension Reactive where Base: LoginWebViewController {
    
    var onCancel: Signal<Void> {
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: nil)
        base.navigationItem.leftBarButtonItem = cancelButton
        
        return cancelButton.rx.tap.asSignal()
    }
}
