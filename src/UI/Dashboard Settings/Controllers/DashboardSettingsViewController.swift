//
//  DashboardSettingsViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 07.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import RxDataSources

import Domain

class DashboardSettingsViewController: EkscesoViewController<DashboardSettingsViewModel>, UITableViewDelegate {
    
    // - MARK: ibOutlet(s)
    @IBOutlet fileprivate weak var ibTableView: UITableView!
    @IBOutlet fileprivate weak var ibLogoutButton: UIButton!
    
    // - MARK: Public Variable(s)
    
    // - MARK: Private Variable(s)
    private let dataSource: RxTableViewSectionedAnimatedDataSource<DashboardSettingsViewModel.FeedSection> = {
        let dataSource = RxTableViewSectionedAnimatedDataSource<DashboardSettingsViewModel.FeedSection>(configureCell: { (dataSource, tableView, indexPath, viewModel) -> UITableViewCell in
            switch viewModel {
            case let viewModel as DashboardSettingsCalendarCellViewModel:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.dashboardSettingsCalendarTableViewCell, for: indexPath) !! "Cell can't be nil"

                cell.viewModel = viewModel

                return cell
            case let viewModel as DashboardSettingsIntervalCellViewModel:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.dashboardSettingsIntervalTableViewCell, for: indexPath) !! "Cell can't be nil"

                cell.viewModel = viewModel

                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.dashboardSettingsTableViewCell, for: indexPath) !! "Cell can't be nil"

                cell.viewModel = viewModel

                return cell
            }
        })

        dataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .fade, reloadAnimation: .fade, deleteAnimation: .fade)

        return dataSource
    }()
    
    // - MARK: View Method(s)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func bind(viewModel: DashboardSettingsViewModel) {
        super.bind(viewModel: viewModel)
        
        viewModel
            .feed
            .drive(ibTableView.rx.items(dataSource: dataSource))
            .dispose(in: disposeBag)
        
        viewModel
            .feed
            .delay(0.2)
            .driveNext { [weak self] (_) in
                let indexPaths = viewModel.initialSelectedIndexes
                
                indexPaths
                    .forEach { (indexPath) in
                        self?.ibTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                    }
            }
            .dispose(in: disposeBag)
        
        ibTableView
            .rx
            .itemSelected
            .subscribeNext { [weak self] tableView, indexPath in
                self?.view.endEditing(true)
                
                tableView.beginUpdates()
                tableView.endUpdates()
            }
            .dispose(in: disposeBag)
        
        ibTableView
            .rx
            .setDelegate(self)
            .dispose(in: disposeBag)
        
        ibLogoutButton
            .rx
            .tap
            .subscribeNext {
                viewModel.didTapLogout()
            }
            .dispose(in: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = R.nib.dashboardSettingsSectionHeader.firstView(owner: nil) !! "View can't be nil"
        view.titleLabel.text = dataSource[section].model.title
        view.imageView.image = dataSource[section].model.imageAsTemplate
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == DashboardSettingsSectionType.interval.rawValue ||
            indexPath.section == DashboardSettingsSectionType.colorMode.rawValue {
            let numberOfItems = tableView.numberOfRows(inSection: indexPath.section)
            
            for index in 0...numberOfItems where index != indexPath.row {
                let indexPath = IndexPath(item: index, section: indexPath.section)
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            let cell = tableView.cellForRow(at: indexPath)
            cell?.becomeFirstResponder()
        }
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == DashboardSettingsSectionType.interval.rawValue {
            return nil
        }
        if indexPath.section == DashboardSettingsSectionType.accounts.rawValue,
            dataSource[indexPath.section].items.count == 1 {
            return nil
        }
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DashboardSettingsTableViewCell.height(forViewModel: dataSource.sectionModels[indexPath.section].items[indexPath.item])
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.blockAnyChanges()
    }
}

// - MARK: private DashboardSettingsViewController extension
private extension DashboardSettingsViewController {
    
    func setup() {
        self.navigationItem.title = "Settings"
        
        self.ibTableView.register(R.nib.dashboardSettingsCalendarTableViewCell)
        self.ibTableView.register(R.nib.dashboardSettingsTableViewCell)
        self.ibTableView.register(R.nib.dashboardSettingsIntervalTableViewCell)
        
        self.ibTableView.allowsMultipleSelection = true
    }
}
