//
//  DashboardSettingsTableViewCellContentView.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM

import Domain
import Shared

class DashboardSettingsTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var ibTitleLabel: EkscesoLabel!
    @IBOutlet private weak var ibDotView: BorderedView!
    @IBOutlet private weak var ibLineHeight: NSLayoutConstraint!
    
    var viewModel: DashboardSettingsCellViewModel! {
        didSet {
            bind(viewModel: viewModel)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ibLineHeight.constant = (1 / UIScreen.main.scale)
        
        NotificationCenter
            .default
            .rx
            .notification(Notification.Name.colorModeChanged)
            .subscribeNext(onColorModeChaged)
            .dispose(in: disposeBag)
    }
    
    func bind(viewModel: DashboardSettingsCellViewModel) {
        viewModel.unblock()
        
        viewModel
            .titleDriver
            .drive(ibTitleLabel.rx.text)
            .dispose(in: disposeBag)
    }
    
    func onColorModeChaged(notification: Notification) {
        self.refreshDot()
    }
    
    private func refreshDot() {
        ibDotView?.backgroundColor = isSelected ? Configuration.Colors.dotSelected.color : .clear
        ibDotView?.border = isSelected == false
        ibDotView?.layoutIfNeeded()
    }
    
    class func height(forViewModel viewModel: DashboardSettingsCellViewModel) -> CGFloat {
        switch viewModel {
        case is DashboardSettingsCalendarCellViewModel:
            return 350
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    override func blockAnyChanges() {
        viewModel?.blockChanges()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.viewModel?.set(selected: isSelected)
        
        self.refreshDot()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
}
