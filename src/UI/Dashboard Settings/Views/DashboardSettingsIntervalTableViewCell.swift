//
//  DashboardSettingsIntervalTableViewCell.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 14.09.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxCocoa
import SwiftDate

import Shared
import Domain

class DashboardSettingsIntervalTableViewCell: DashboardSettingsTableViewCell {
    
    @IBOutlet private weak var ibFromTextField: UITextField!
    
    override func bind(viewModel: DashboardSettingsCellViewModel) {
        super.bind(viewModel: viewModel)
        
        guard let viewModel = viewModel as? DashboardSettingsIntervalCellViewModel else { return }
        
        viewModel
            .isCustomDateViewHidden
            .drive(ibFromTextField.rx.isHidden)
            .dispose(in: disposeBag)
        
        let increment = DateComponents.create {
            $0.month = 1
        }
        
        let fromDate = Date().dateAtStartOf(.year).inDefaultRegion()
        let toDate = Date().dateAtEndOf(.year).inDefaultRegion()
        
        let dates = DateInRegion.enumerateDates(from: fromDate, to: toDate, increment: increment)
        
        let feed = Driver.just(dates)
        
        let monthPicker = UIPickerView()
            
        feed
            .drive(monthPicker.rx.items) { row, item, view in
                let label = UILabel()
                label.font = FontFamily.AvenirNext.regular.font(withSize: 22)
                label.text = item.toString(.custom("MMMM"))
                label.sizeToFit()
                
                return label
            }
            .dispose(in: disposeBag)
        
        viewModel
            .customMonth
            .withLatestFrom(feed) { ($0, $1) }
            .driveNext { (month, feed) in
                guard let index = feed.firstIndex(of: month.inDefaultRegion()) else { return }
                
                monthPicker.selectRow(index, inComponent: 0, animated: false)
            }
            .dispose(in: disposeBag)
        
        ibFromTextField.inputView = monthPicker
        
        viewModel
            .customMonth
            .map { $0.monthName(.default) }
            .drive(ibFromTextField.rx.text)
            .dispose(in: disposeBag)
        
        let selectedMonth: Driver<Date> = monthPicker
            .rx
            .modelSelected(DateInRegion.self)
            .asDriver()
            .map { $0.first?.date }
            .filterNil()
        
        viewModel.bind(customMonth: selectedMonth, disposeBag: disposeBag)
    }
    
    override func becomeFirstResponder() -> Bool {
        guard let viewModel = viewModel as? DashboardSettingsIntervalCellViewModel else { return false }
        
        if viewModel.isCustomInterval {
            DispatchQueue
                .main
                .asyncAfter(deadline: .now() + 0.1) { [weak self] in
                    self?.ibFromTextField.becomeFirstResponder()
            }
            
            return true
        }
        
        return false
    }
}
