//
//  DashboardSettingsSectionHeader.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 27.07.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import Domain

import Domain
import Shared

class DashboardSettingsSectionHeader: EkscesoView {

    @IBOutlet weak var titleLabel: EkscesoLabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet private weak var ibLineHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setup()
    }
    
    override func onColorModeChaged(notification: Notification) {
        super.onColorModeChaged(notification: notification)
        
        self.setup()
    }
    
    private func setup() {
        self.activityIndicator.color = Configuration.Colors.brand.color
        self.backgroundColor = Configuration.Colors.primaryBackground.color
        self.ibLineHeight.constant = (1 / UIScreen.main.scale)
        self.imageView.tintColor = Configuration.Colors.dashboardSettingsSectionImageTint.color
    }
}
