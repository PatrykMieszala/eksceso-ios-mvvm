//
//  DashboardSettingsCalendarTableViewCell.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 29.06.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import FSCalendar
import SwiftDate

import Domain
import Shared

class DashboardSettingsCalendarTableViewCell: DashboardSettingsTableViewCell, FSCalendarDataSource, FSCalendarDelegate {
    
    // - MARK: ibOutlet(s)
    @IBOutlet fileprivate weak var ibCalendar: EkscesoFSCalendar!
    @IBOutlet fileprivate weak var ibSelectionDot: UIView!
    @IBOutlet fileprivate weak var ibTodayDot: UIView!
    
    // - MARK: Public Variable(s)
    private var calendarViewModel: DashboardSettingsCalendarCellViewModel! {
        return viewModel as? DashboardSettingsCalendarCellViewModel
    }
    
    // - MARK: Private Variable(s)
    
    // - MARK: View Method(s)    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
        self.setupUI()
    }
    
    override func bind(viewModel: DashboardSettingsCellViewModel) {
        calendarViewModel
            .selectedDates
            .forEach { (date) in
                ibCalendar.select(date, scrollToDate: false)
        }
    }
    
    override func onColorModeChaged(notification: Notification) {
        super.onColorModeChaged(notification: notification)
        self.setupUI()
    }
    
    // MARK: - FSCalendarDataSource
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return calendarViewModel.minDate
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return calendarViewModel.maxDate
    }
    
    // MARK: - FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        calendarViewModel.didSelect(date: date)
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        calendarViewModel.didDeselect(date: date)
    }
}

private extension DashboardSettingsCalendarTableViewCell {
    
    func setup() {
        ibCalendar.dataSource = self
        ibCalendar.delegate = self
        
        ibCalendar.allowsMultipleSelection = true
        
        ibCalendar.firstWeekday = 2
        
        ibCalendar.allowsMultipleSelection = true
    }
    
    func setupUI() {
        
        let appearance = ibCalendar.appearance
        
        appearance.selectionColor = Configuration.Colors.selection.color
        
        appearance.weekdayTextColor = Configuration.Colors.weekdayText.color
        appearance.weekdayFont = Configuration.Fonts.primaryText.with(size: 13)
        appearance.headerTitleColor = Configuration.Colors.headerTitle.color
        appearance.headerTitleFont = Configuration.Fonts.primaryText.with(size: 18)
        appearance.headerDateFormat = "MMMM yyyy"
        appearance.borderRadius = 1.0
        appearance.headerMinimumDissolvedAlpha = 0.2
        appearance.titleDefaultColor = Configuration.Colors.titleDefault.color
        appearance.titleSelectionColor = Configuration.Colors.titleSelection.color
        
        ibSelectionDot.backgroundColor = appearance.selectionColor
        ibTodayDot.backgroundColor = appearance.todayColor
    }
}

class EkscesoFSCalendar: FSCalendar {
    
    override var gregorian: Calendar! {
        get {
            var calendar = Calendar.current
            calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            
            return calendar
        }
    }
}
