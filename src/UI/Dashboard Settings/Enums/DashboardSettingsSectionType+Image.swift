//
//  DashboardSettingsSectionType+Image.swift
//  UI
//
//  Created by Patryk Mieszała on 30/01/2019.
//  Copyright © 2019 Patryk Mieszała. All rights reserved.
//

import Domain

extension DashboardSettingsSectionType {
    
    var image: UIImage? {
        switch self {
        case .accounts: return R.image.accountsIcon2()
        case .interval: return R.image.intervalIcon()
        case .daysOff: return R.image.daysOffIcon()
        case .colorMode: return nil
        }
    }
    
    var imageAsTemplate: UIImage? {
        return image?.withRenderingMode(.alwaysTemplate)
    }
}
