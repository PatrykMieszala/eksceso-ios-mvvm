//
//  SplashViewController.swift
//  Eksceso
//
//  Created by Patryk Mieszała on 22.05.2018.
//  Copyright © 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxMVVM
import Domain

class SplashViewController: EkscesoViewController<SplashViewModel> {
    
    // - MARK: ibOutlet(s)
    
    // - MARK: Public Variable(s)
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // - MARK: Private Variable(s)
    
    // - MARK: View Method(s)
}

